<?php
function ajoutVetement(){
require('../modele/connexionDB.php');
$db = connectBDD();
if(isset($_POST['nom'])){
    $photo = $_FILES['photo']['name'];
    $photo2 = $_FILES['photo2']['name'];
    $photo3 = $_FILES['photo3']['name'];
    $photo4 = $_FILES['photo4']['name'];
    $photo5 = $_FILES['photo5']['name'];
    $photo6 = $_FILES['photo6']['name'];
    $photo7 = $_FILES['photo7']['name'];
    $photo8 = $_FILES['photo8']['name'];

    $couleur = $_FILES['couleur']['name'];
    $couleur2 = $_FILES['couleur2']['name'];
    $couleur3 = $_FILES['couleur3']['name'];
    $couleur4 = $_FILES['couleur4']['name'];
    $couleur5 = $_FILES['couleur5']['name'];
    $couleur6 = $_FILES['couleur6']['name'];
    $couleur7 = $_FILES['couleur7']['name'];
    $couleur8 = $_FILES['couleur8']['name'];


    if(isset($_POST['buttonAjout']))
    {
      if(isset($_POST['XS']))
      {

          $XS = 1;
          unset($_POST['button']);
      }
      else
      {
        $XS = 0;
      }

      if(isset($_POST['S']))
      {
            $S = 1;
            unset($_POST['button']);
      }
      else
      {
        $S = 0;
      }

      if(isset($_POST['M']))
      {
          $M = 1;
          unset($_POST['button']);
      }
      else
      {
        $M = 0;
      }

      if(isset($_POST['L']))
      {
          $L = 1;
          unset($_POST['button']);
      }
      else
      {
        $L = 0;
      }

      if(isset($_POST['XL']))
      {
          $XL = 1;
          unset($_POST['button']);
      }
      else
      {
        $XL = 0;
      }

      if(isset($_POST['XXL']))
      {
          $XXL = 1;
          unset($_POST['button']);
      }
      else
      {
        $XXL = 0;
      }

      if(isset($_POST['Autre']))
      {
          $Autre = 1;
          unset($_POST['button']);
      }
      else
      {
        $Autre = 0;
      }
    }




    $req = $db->prepare('INSERT INTO vetements(nom,nbr,couleur,couleur2,couleur3,couleur4,couleur5,couleur6,couleur7,couleur8,descr,prix,type,photo,photo2,photo3,photo4,photo5,photo6,photo7,photo8,afficher,XS,S,M,L,XL,XXL,Autre)
    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
    $req->execute(array($_POST['nom'],$_POST['nbr'],$couleur,$couleur2,$couleur3,$couleur4,$couleur5,$couleur6,$couleur7,$couleur8,$_POST['descr'],$_POST['prix'],$_POST['type'],$photo,$photo2,$photo3,$photo4,$photo5,$photo6,$photo7,$photo8,'1',
    $XS,$S,$M,$L,$XL,$XXL,$Autre));
}

// ----------------------------------------------------------------------- PHOTO 1 ------------------------------------------------------------------------------------

if(isset($_FILES["photo"]["name"])) {
    //répertoire de déstination
    $target_dir = "../articles/";
    $target_file = $target_dir . basename($_FILES["photo"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["photo"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["photo"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: ../controleur/vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}



// ----------------------------------------------------------------------- PHOTO 2 ------------------------------------------------------------------------------------

if(isset($_FILES["photo2"]["name"])) {
    //répertoire de déstination
    $target_dir = "../articles/";
    $target_file = $target_dir . basename($_FILES["photo2"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["photo2"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["photo2"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["photo2"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

//--------------------------------------------------------------------------PHOTO 3-----------------------------------------------------------------------------------
if(isset($_FILES["photo3"]["name"])) {
    //répertoire de déstination
    $target_dir = "../articles/";
    $target_file = $target_dir . basename($_FILES["photo3"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["photo3"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["photo3"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["photo3"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

// ----------------------------------------------------------------------- PHOTO 4 ------------------------------------------------------------------------------------

if(isset($_FILES["photo4"]["name"])) {
    //répertoire de déstination
    $target_dir = "../articles/";
    $target_file = $target_dir . basename($_FILES["photo4"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["photo4"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["photo4"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["photo4"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: ../controleur/vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}



// ----------------------------------------------------------------------- PHOTO 5 ------------------------------------------------------------------------------------

if(isset($_FILES["photo5"]["name"])) {
    //répertoire de déstination
    $target_dir = "../articles/";
    $target_file = $target_dir . basename($_FILES["photo5"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["photo5"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["photo5"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["photo5"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

//--------------------------------------------------------------------------PHOTO 6-----------------------------------------------------------------------------------
if(isset($_FILES["photo6"]["name"])) {
    //répertoire de déstination
    $target_dir = "../articles/";
    $target_file = $target_dir . basename($_FILES["photo6"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["photo6"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["photo6"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["photo6"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

// ----------------------------------------------------------------------- PHOTO 7 ------------------------------------------------------------------------------------

if(isset($_FILES["photo7"]["name"])) {
    //répertoire de déstination
    $target_dir = "../articles/";
    $target_file = $target_dir . basename($_FILES["photo7"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["photo7"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["photo7"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["photo7"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

//--------------------------------------------------------------------------PHOTO 8-----------------------------------------------------------------------------------
if(isset($_FILES["photo8"]["name"])) {
    //répertoire de déstination
    $target_dir = "../articles/";
    $target_file = $target_dir . basename($_FILES["photo8"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["photo8"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["photo8"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["photo8"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

//--------------------------------------------------------------------------COULEUR 1-----------------------------------------------------------------------------------

if(isset($_FILES["couleur"]["name"])) {
    //répertoire de déstination
    $target_dir = "../couleur/";
    $target_file = $target_dir . basename($_FILES["couleur"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["couleur"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }
    // le poid de l'image

    if ($_FILES["couleur"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["couleur"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: ../controleur/vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}



// ----------------------------------------------------------------------- COULEUR 2 ------------------------------------------------------------------------------------

if(isset($_FILES["couleur2"]["name"])) {
    //répertoire de déstination
    $target_dir = "../couleur/";
    $target_file = $target_dir . basename($_FILES["couleur2"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["couleur2"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    if (file_exists($target_file)) {
        $message = "Erreur! le fichier image existe déjà.";
        echo $message;
        $uploadOk = 0;
    }

    // le poid de l'image
    // if ($_FILES["photo2"]["size"] > 10000000) {
    //     $message = "Le fichier selectionné est trop volumineux.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["couleur2"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

//-------------------------------------------------------------------------- COULEUR 3 -----------------------------------------------------------------------------------
if(isset($_FILES["couleur3"]["name"])) {
    //répertoire de déstination
    $target_dir = "../couleur/";
    $target_file = $target_dir . basename($_FILES["couleur3"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["couleur3"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["couleur3"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["couleur3"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

// ----------------------------------------------------------------------- COULEUR 4 ------------------------------------------------------------------------------------

if(isset($_FILES["couleur4"]["name"])) {
    //répertoire de déstination
    $target_dir = "../couleur/";
    $target_file = $target_dir . basename($_FILES["couleur4"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["couleur4"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["couleur4"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["couleur4"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: ../controleur/vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}



// ----------------------------------------------------------------------- COULEUR 5 ------------------------------------------------------------------------------------

if(isset($_FILES["couleur5"]["name"])) {
    //répertoire de déstination
    $target_dir = "../couleur/";
    $target_file = $target_dir . basename($_FILES["couleur5"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["couleur5"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["couleur5"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["couleur5"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

//-------------------------------------------------------------------------- COULEUR 6 -----------------------------------------------------------------------------------
if(isset($_FILES["couleur6"]["name"])) {
    //répertoire de déstination
    $target_dir = "../couleur/";
    $target_file = $target_dir . basename($_FILES["couleur6"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["couleur6"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["couleur6"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["couleur6"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

// ----------------------------------------------------------------------- COULEUR 7 ------------------------------------------------------------------------------------

if(isset($_FILES["couleur7"]["name"])) {
    //répertoire de déstination
    $target_dir = "../couleur/";
    $target_file = $target_dir . basename($_FILES["couleur7"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["couleur7"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["couleur7"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["couleur7"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}

//-------------------------------------------------------------------------- COULEUR 8 -----------------------------------------------------------------------------------
if(isset($_FILES["couleur8"]["name"])) {
    //répertoire de déstination
    $target_dir = "../couleur/";
    $target_file = $target_dir . basename($_FILES["photo8"]["name"]);
    //on initialise la variable update ok
    $uploadOk = 1;
    //on recup l'extention du fichier
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    //on a cliqué sur le bouton qui s'appel submit
    if(isset($_POST["button"])) {
        //fichier image?
        $check = getimagesize($_FILES["couleur8"]["tmp_name"]);
        if($check !== false) {
            $uploadOk = 1;
        } else {
            $message = "Le fichier n'est pas une image valide.";
            echo $message;
            $uploadOk = 0;
        }
    }

    // le fichier existe déjà?
    // if (file_exists($target_file)) {
    //     $message = "Erreur! le fichier image existe déjà.";
    //     echo $message;
    //     $uploadOk = 0;
    // }

    // le poid de l'image
    if ($_FILES["couleur8"]["size"] > 10000000) {
        $message = "Le fichier selectionné est trop volumineux.";
        echo $message;
        $uploadOk = 0;
    }
    // les formats autorisés
    if($imageFileType != "jpg" &&$imageFileType != "JPG"&& $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg" && $imageFileType != "JPEG" && $imageFileType != "gif" && $imageFileType != "GIF") {

        $message = "Les images doivent etre au format: JPG, JPEG, PNG ou GIF.";
        echo $message;
        $uploadOk = 0;
    }
    // erreur
    if ($uploadOk == 0) {
        $message = "Erreur! impossible d'ajouter l'image.";
        echo $message;

    // tt c'est bien passé
    } else {
        if (move_uploaded_file($_FILES["couleur8"]["tmp_name"], $target_file)) {

            $message = "Image ajoutée avec succès.";
            echo $message;
            header("Location: vetements.php");

        } else {
            $message = "Erreur inconnue! Merci de retenter l'ajout plus tard ou de contacter l'administrateur.";
            echo $message;
        }
    }
}
ob_end_flush();
}
?>
