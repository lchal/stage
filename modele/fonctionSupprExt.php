<?php
function SupExt ($file_name) {
  // met le point '.' dans la variable $trouve_moi
  $trouve_moi = ".";

  // cherche la postion du '.'
  $position = strpos($file_name, $trouve_moi);

  // enleve l'extention, tout ce qui se trouve apres le '.'
  return substr($file_name, 0, $position);
}
?>
