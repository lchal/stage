<?php
session_start();
include "../vue/vueProfil.php";
include "../vue/enTete.html.php";


if(!isset($_SESSION['user']) || $_SESSION['admin'] == "0")
{
  header('Location: ../index.php');
  die();
}

if(isset($_POST['taille']))
{
  if(isset($_GET['idV']))
  {
    $idV = $_GET['idV'];

    if(isset($_POST['XS']))
    {
      $stockV = $db -> prepare("UPDATE vetements SET XS = ? WHERE idV = ?");
      $stockV -> execute(array(1, $idV));
    }
    else
    {
      $stockV = $db -> prepare("UPDATE vetements SET XS = ? WHERE idV = ?");
      $stockV -> execute(array(0, $idV));
    }

    if(isset($_POST['S']))
    {
      $stockV = $db -> prepare("UPDATE vetements SET S = ? WHERE idV = ?");
      $stockV -> execute(array(1, $idV));
    }
    else
    {
      $stockV = $db -> prepare("UPDATE vetements SET S = ? WHERE idV = ?");
      $stockV -> execute(array(0, $idV));
    }

    if(isset($_POST['M']))
    {
      $stockV = $db -> prepare("UPDATE vetements SET M = ? WHERE idV = ?");
      $stockV -> execute(array(1, $idV));
    }
    else
    {
      $stockV = $db -> prepare("UPDATE vetements SET M = ? WHERE idV = ?");
      $stockV -> execute(array(0, $idV));
    }

    if(isset($_POST['L']))
    {
      $stockV = $db -> prepare("UPDATE vetements SET L = ? WHERE idV = ?");
      $stockV -> execute(array(1, $idV));
    }
    else
    {
      $stockV = $db -> prepare("UPDATE vetements SET L = ? WHERE idV = ?");
      $stockV -> execute(array(0, $idV));
    }

    if(isset($_POST['XL']))
    {
      $stockV = $db -> prepare("UPDATE vetements SET XL = ? WHERE idV = ?");
      $stockV -> execute(array(1, $idV));
    }
    else
    {
      $stockV = $db -> prepare("UPDATE vetements SET XL = ? WHERE idV = ?");
      $stockV -> execute(array(0, $idV));
    }

    if(isset($_POST['XXL']))
    {
      $stockV = $db -> prepare("UPDATE vetements SET XXL = ? WHERE idV = ?");
      $stockV -> execute(array(1, $idV));
    }
    else
    {
      $stockV = $db -> prepare("UPDATE vetements SET XXL = ? WHERE idV = ?");
      $stockV -> execute(array(0, $idV));
    }

    if(isset($_POST['Autre']))
    {
      $stockV = $db -> prepare("UPDATE vetements SET Autre = ? WHERE idV = ?");
      $stockV -> execute(array(1, $idV));
    }
    else
    {
      $stockV = $db -> prepare("UPDATE vetements SET Autre = ? WHERE idV = ?");
      $stockV -> execute(array(0, $idV));
    }
  }

  header('Location: ../controleur/stock.php');die();
}



?>
