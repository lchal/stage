<?php
use Dompdf\Dompdf;
use Dompdf\Options;
require('../modele/connexionDB.php');
require_once '../dompdf/autoload.inc.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require('../mail/PHPMailer/Exception.php');
require('../mail/PHPMailer/PHPMailer.php');
require('../mail/PHPMailer/SMTP.php');



$db = connectBDD();
session_start();

if(isset($_GET['num_com']))
{
  $getNum_Com = $_GET['num_com'];
  $commandesU= $db->query("SELECT * FROM commandes WHERE num_com = '$getNum_Com'");
  $commandesU = $commandesU->fetchAll();

  ob_start();
  require_once '../vue/pdf_Facture.php';
  $html = ob_get_contents();



$options = new Options();
$options->set('defaultFont', 'Courier');
$dompdf = new Dompdf($options);
$dompdf->loadHtml($html);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();
$facture = $dompdf->output();
$fichier = $getNum_Com;
file_put_contents("../factures/$getNum_Com.pdf", $facture);



$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->CharSet = 'UTF-8';
    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'modo.midinet@gmail.com';                     //SMTP username
    $mail->Password   = 'ModoMidinet1!';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
    $mail->Port       = 587;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    $mail->addAttachment("../factures/$getNum_Com.pdf", 'Facture.pdf');         //Add attachments

    //Recipients
    $mail->setFrom('modo.midinet@gmail.com', 'Facture commande Midi-Net');
    $mail->addAddress('lucas.chalscl@gmail.com');    //Add a recipient
    //$mail->addAddress('clement.fellah14@gmail.com');

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = 'Facture Les Midi-Net\' ';
    $mail->Body= "Voici votre facture de la commande $getNum_Com";


    $mail->send();
  }
  catch (Exception $e)
  {
      echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
  }
  header('Location:../controleur/listeCommande.php');

}
ob_end_clean();
