<?php
  require('../modele/connexionDB.php');
  $db = connectBDD();
  session_start();

  if(!empty($_POST['Prenom']) && !empty($_POST['Nom']) && !empty($_POST['Adresse']) && !empty($_POST['CP']) && !empty($_POST['Tel']))
  {
    // Patch XSS

    $Prenom = htmlspecialchars($_POST['Prenom']);
    $Nom = htmlspecialchars($_POST['Nom']);
    $Adresse = htmlspecialchars($_POST['Adresse']);
    $CP = htmlspecialchars($_POST['CP']);
    $Tel = htmlspecialchars($_POST['Tel']);



    function generateRandomString($length = 8)
    {
      $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++)
      {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }


    // Echo the random string.
    // Optionally, you can give it a desired string length.
    $num_com = generateRandomString();


    $insert = $db->prepare('INSERT INTO info_commandes(prenom, nom, tel, adresse, CP, num_com, commande_valider, email_user, total_Com) VALUES(:prenom, :nom, :tel, :adresse, :CP, :num_com, :commande_valider, :email_user, :total_Com)');
    $insert->execute(array(
      'prenom' => $Prenom,
      'nom' => $Nom,
      'tel' => $Tel,
      'adresse' => $Adresse,
      'CP' => $CP,
      'num_com' => $num_com,
      'commande_valider' => '0',
      'email_user' => $_SESSION['user'],
      'total_Com' => 0
    ));

    if (isset($_SESSION['user']))
    {
      $user = $_SESSION['user'];

      $panier= $db->query("SELECT * FROM panier where user_email = '$user'");
      $donnes = $panier->fetchAll();

      foreach($donnes as $donnes):
        $idP = $donnes['product_idV'];
        $idD = $donnes['product_idA'];
        $id = $donnes['id'];
        $user_email = $donnes['user_email'];
        $couleurProduit = $donnes['couleurProduit'];
        $Taille = $donnes['Taille'];

        if($idP !=0)
        {
          $produitV = $db -> query("SELECT * FROM vetements where idV = '$idP'");
          $panierV = $produitV->fetchAll();
          $supprV = $db ->prepare('UPDATE vetements set nbr = nbr-1 WHERE idV = ? ');
          $supprV->execute(array($idP));
          foreach($panierV as $panierV):
            $insertV = $db->prepare('INSERT INTO commandes(Com_idA, Com_idV, email_user, num_com, couleurProduit, Taille) VALUES(:Com_idA, :Com_idV, :email_user, :num_com, :couleurProduit, :Taille)');
            $insertV->execute(array(
              'Com_idA' => "0",
              'Com_idV' => $idP,
              'email_user' => $user_email,
              'num_com' => $num_com,
              'couleurProduit' => $couleurProduit,
              'Taille' => $Taille
            ));
            $total = $total+$panierV['prix'];
            $total = number_format((float)$total, 2, '.', '');


          endforeach;

        }
        if($idD !=0)
          {
            $produitA = $db -> query("SELECT * FROM accessoires where idA = '$idD'");
            $panierA = $produitA->fetchAll();
            $supprA = $db ->prepare('UPDATE accessoires set nbr = nbr-1 WHERE idA = ? ');
            $supprA->execute(array($idD));
            foreach($panierA as $panierA):
            $insertA = $db->prepare('INSERT INTO commandes(Com_idA, Com_idV, email_user, num_com, couleurProduit, Taille) VALUES(:Com_idA, :Com_idV, :email_user, :num_com, :couleurProduit, :Taille)');
            $insertA->execute(array(
            'Com_idA' => $idD,
            'Com_idV' => "0",
            'email_user' => $user_email,
            'num_com' => $num_com,
            'couleurProduit' => $couleurProduit,
            'Taille' => $Taille
            ));
            $total = $total+$panierA['prix'];
            $total = number_format((float)$total, 2, '.', '');
            endforeach;

          }
        $NewPntFidelite = $db -> prepare("UPDATE info_commandes SET total_Com = ? WHERE num_com = ?");
        $NewPntFidelite -> execute(array($_SESSION['total'], $num_com));

        endforeach;


      }
  }
  $_SESSION['user'] = $user_email;
  $_SESSION['numCom'] = $num_com;
  header("Location:validePaiement.php");
?>
