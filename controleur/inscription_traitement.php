<?php
    ob_start();
    //Import PHPMailer classes into the global namespace
    //These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    //Load Composer's autoloader
    require('../mail/PHPMailer/Exception.php');
    require('../mail/PHPMailer/PHPMailer.php');
    require('../mail/PHPMailer/SMTP.php');

    require('../modele/connexionDB.php');
    $db = connectBDD();
    session_start();

    // Si les variables existent et qu'elles ne sont pas vides
    if(!empty($_POST['pseudo']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['password_retype']))
    {
        // Patch XSS

        $pseudo = htmlspecialchars($_POST['pseudo']);
        $email = htmlspecialchars($_POST['email']);
        $password = htmlspecialchars($_POST['password']);
        $password_retype = htmlspecialchars($_POST['password_retype']);

        // On vérifie si l'utilisateur existe
        $check = $db->prepare('SELECT pseudo, email, password FROM utilisateurs WHERE email = ?');
        $check->execute(array($email));
        $data = $check->fetch();
        $row = $check->rowCount();

        $email = strtolower($email); // on transforme toute les lettres majuscule en minuscule pour éviter que Foo@gmail.com et foo@gmail.com soient deux compte différents ..

        // Si la requete renvoie un 0 alors l'utilisateur n'existe pas
        if($row == 0){
            if(strlen($pseudo) <= 100){ // On verifie que la longueur du pseudo <= 100
                if(strlen($email) <= 100){ // On verifie que la longueur du mail <= 100
                    if(filter_var($email, FILTER_VALIDATE_EMAIL)){ // Si l'email est de la bonne forme
                        if($password === $password_retype){ // si les deux mdp saisis sont bon

                            // On hash le mot de passe avec Bcrypt, via un coût de 12
                            $cost = ['cost' => 12];
                            $password = password_hash($password, PASSWORD_BCRYPT, $cost);

                            // On stock l'adresse IP
                            $ip = $_SERVER['REMOTE_ADDR'];

                            /*
                                Pour ceux qui souhaite mettre en place un système de mot de passe oublié, pensez à mettre le champ token dans votre requête
                                N'oubliez pas également d'ajouter le même champs à votre table utilisateurs
                                $insert = $bdd->prepare('INSERT INTO utilisateurs(pseudo, email, password, ip, token) VALUES(:pseudo, :email, :password, :ip, :token)');
                                $insert->execute(array(
                                    'pseudo' => $pseudo,
                                    'email' => $email,
                                    'password' => $password,
                                    'ip' => $ip,
                                    'token' =>  bin2hex(openssl_random_pseudo_bytes(24))
                                ));
                              */
                            // On insère dans la base de données
                            $admin = 0;
                            $cle = rand(10000, 90000);
                            $verifier =0;
                            $pntFidelite =0;
                            $insert = $db->prepare('INSERT INTO utilisateurs(pseudo, email, password, ip, admin, cle, verifier, pntFidelite) VALUES(:pseudo, :email, :password, :ip, :admin, :cle, :verifier, :pntFidelite)');
                            $insert->execute(array(
                                'pseudo' => $pseudo,
                                'email' => $email,
                                'password' => $password,
                                'ip' => $ip,
                                'admin' => $admin,
                                'cle' => $cle,
                                'verifier' => $verifier,
                                'pntFidelite' => $pntFidelite

                            ));
                            // On redirige avec le message de succès




                            //Create an instance; passing `true` enables exceptions
                            $mail = new PHPMailer(true);

                            try {
                                //Server settings
                                $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                                $mail->isSMTP();                                            //Send using SMTP
                                $mail->CharSet = 'UTF-8';
                                $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
                                $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                                $mail->Username   = 'modo.midinet@gmail.com';                     //SMTP username
                                $mail->Password   = 'ModoMidinet1!';                               //SMTP password
                                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
                                $mail->Port       = 587;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

                              //  $mail->addAttachment('../images/cacachal.jpg');         //Add attachments

                                //Recipients
                                $mail->setFrom('modo.midinet@gmail.com', 'Confirmation mail Midi-Net');
                                $mail->addAddress('lucas.chalscl@gmail.com');    //Add a recipient
                                $mail->addAddress('clement.fellah14@gmail.com');

                                //Content
                                $mail->isHTML(true);                                  //Set email format to HTML
                                $mail->Subject = 'Lien de confirmation d\'email ';
                                $mail->Body= 'http://lucas/stage/vue/vueVerif_email.php?email='.$email.'&cle='.$cle.'<br><br>http://fellah/stage/vue/vueVerif_email.php?email='.$email.'&cle='.$cle;


                                $mail->send();

                                echo 'Message has been sent';


                            }
                            catch (Exception $e)
                            {
                                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                            }
                                header('Location: identification.php?reg_err=ok');die();

                            die();
                        }else{ header('Location: identification.php?reg_err=password'); die();}
                    }else{ header('Location: identification.php?reg_err=email'); die();}
                }else{ header('Location: identification.php?reg_err=email_length'); die();}
            }else{ header('Location: identification.php?reg_err=pseudo_length'); die();}
        }else{ header('Location: identification.php?reg_err=already'); die();}
    }
    ob_end_flush();
?>
