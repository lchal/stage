<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>#</title>
        <link rel="stylesheet" href="/stage/css/enTete.css">
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    </head>

    <body>
        <nav>
            <div class="logo">
                <a href="/stage/index.php"><img class="logoSite" src="/stage/images/Logo_Midi_Net_TE.png"></img></a>
            </div>
            <ul class="nav-links">
                <li>
                    <a class="produit" href="/stage/controleur/Vetements.php">Vêtements</a>
                </li>
                <li>
                    <a class="produit" href="/stage/controleur/Accessoires.php">Accessoires</a>
                </li>
                <li>
                    <a class="panier" href="/stage/controleur/panier.php">Panier</a>
                </li>
                <li>
                <?php
                if(!isset($_SESSION['user']))
                {
                    echo "<a href=\"/stage/controleur/identification.php\">Connexion</a>";
                }
                else
                {
                    echo "<a href=\"/stage/controleur/infoPerso.php\">Profil</a>";
                }
                ?>
               </li>
               <li>
                 <?php
                 if(isset($_SESSION['user']))
                 {
                   echo "<a href=\"/stage/controleur/deconnexion.php\"><img src=\"/stage/images/deconnexion1.png\" title=\"Déconnexion\"> </img></a>";
                 }
                 ?>
               </li>
            </ul>
            <div class="burger">
                <div class="line1"></div>
                <div class="line2"></div>
                <div class="line3"></div>
            </div>
            <div class="bande">
                <h5></h5>
            </div>
        </nav>


        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>

        <script type="text/javascript">
            const navSlide = () => {
            const burger = document.querySelector('.burger');
            const nav = document.querySelector('.nav-links');
            const navLinks = document.querySelectorAll('.nav-links li');

            burger.addEventListener('click',()=>{
                //Toggle nav
                nav.classList.toggle('nav-active');
                //Animate Links
                navLinks.forEach((link, index) => {
                    if (link.style.animaton) {
                        link.style.animaton = '';
                        }
                        else {
                            link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7 + 0.5}s`;
                        }
                    });
                    //Burger animation
                    burger.classList.toggle('toggle');
                });
            }
            navSlide();
        </script>

        <script type="text/javascript">
            $(window).on('scroll', function(){
                if($(window).scrollTop() > 15){
                    $('nav').addClass('scroll');
                }
                else{
                    $('nav').removeClass('scroll');
                }
            });
        </script>
    </body>
</html>
