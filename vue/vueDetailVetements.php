<?php
ob_start();
require('../modele/connexionDB.php');
$db = connectBDD();

if(isset($_GET['idV']) AND !empty($_GET['idV'])) {
    $get_idV = htmlspecialchars($_GET['idV']);
    $vetements = $db->prepare('SELECT * FROM vetements WHERE idV = ?');
    $utilisateurs = $db->prepare('SELECT * FROM utilisateurs');
    $utilisateurs = $utilisateurs->fetch();
    $vetements->execute(array($get_idV));
    if ($vetements->rowCount() == 1) {
        $vetements = $vetements->fetch();
        $nom = $vetements['nom'];
        $descr = $vetements['descr'];
        $nbr = $vetements['nbr'];
        // $taille = $vetements['taille'];
        $couleur = $vetements['couleur'];
        $couleur2 = $vetements['couleur2'];
        $couleur3 = $vetements['couleur3'];
        $couleur4 = $vetements['couleur4'];
        $couleur5 = $vetements['couleur5'];
        $couleur6 = $vetements['couleur6'];
        $couleur7 = $vetements['couleur7'];
        $couleur8 = $vetements['couleur8'];
        $prix = $vetements['prix'];
        $photo = $vetements['photo'];
        $photo2 = $vetements['photo2'];
        $photo3 = $vetements['photo3'];
        $photo4 = $vetements['photo4'];
        $photo5 = $vetements['photo5'];
        $photo6 = $vetements['photo6'];
        $photo7 = $vetements['photo7'];
        $photo8 = $vetements['photo8'];
        $idV = $vetements['idV'];
        $XS =$vetements['XS'];
        $S =$vetements['S'];
        $M =$vetements['M'];
        $L =$vetements['L'];
        $XL =$vetements['XL'];
        $XXL =$vetements['XXL'];
        $Autre =$vetements['Autre'];

        if($XS ==1){
          $TailleXS = "XS";
        }
        if($S ==1){
          $TailleS = "S";
        }
        if($M ==1){
          $TailleM = "M";
        }
        if($L ==1){
          $TailleL = "L";
        }
        if($XL ==1){
          $TailleXL = "XL";
        }
        if($XXL ==1){
          $TailleXXL = "XXL";
        }
        if($Autre ==1){
          $TailleAutre = "Autre";
        }
    }
    else {
        die('Cet article n\'existe pas !');
    }
}

if(!isset($_SESSION['panier']))
    $_SESSION['panier'] = [];
?>


<!DOCTYPE html>
<html lang="fr">
    <head>
    <title>Détail Vêtements</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="../css/vueDetail.css">


    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    </head>
    <body>
        <!-- Slideshow container -->
        <div class="slideshow-container">

        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo\" alt=\"rover\" />";?>
        </div>


        <?php if(!empty($photo2)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo2\" alt=\"rover\" />";?>
            </div>
        <?php } ?>


        <?php if(!empty($photo3)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo3\" alt=\"rover\" />";?>
            </div>
        <?php } ?>


        <?php if(!empty($photo4)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo4\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <?php if(!empty($photo5)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo5\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <?php if(!empty($photo6)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo6\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <?php if(!empty($photo7)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo7\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <?php if(!empty($photo8)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo8\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>

        <div class="infos">
            <div class="detailNom">
                <h1><?=$nom?></h1>
            </div>

            <div class="container-Detail">
                <div class="detailDescr">
                    <h3>Description </h3>
                    <?=$descr?>
                </div>

                <div class="ajoutPanier">
                  <form method="post" enctype="multipart/form-data">
                  <div class="detailCouleur">
                      <h3>Couleur(s)</h3>
                      <div class="couleurP">
                        <style>
                        input[type=checkbox][name=couleurProduit] {
                          -webkit-appearance: none;
                          background: url("../couleur/<?=$couleur?>");
                          border: 1px solid #cacece;
                          box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                          padding: 9px;      
                          position: relative;
                        }

                        input[type=checkbox][name=couleurProduit]:checked {
                          -webkit-appearance: none;
                          border: 2px solid black;
                        }
                        </style>
                      <input type="checkbox" name="couleurProduit" value="<?="$couleur";?>">
                      <br></input>
                      </div>

                      <?php if(!empty($couleur2)) { ?>
                          <div class="couleurP">
                            <style>
                            input[type=checkbox][name=couleurProduit2] {
                              -webkit-appearance: none;
                              background: url("../couleur/<?=$couleur2?>");
                              border: 1px solid #cacece;
                              box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                              padding: 9px;
                              position: relative;
                            }

                            input[type=checkbox][name=couleurProduit2]:checked {
                              -webkit-appearance: none;
                              border: 2px solid black;
                            }
                            </style>
                          <input type="checkbox" name="couleurProduit2" value="<?="$couleur2";?>"><br></input>
                          </div>
                      <?php } ?>


                      <?php if(!empty($couleur3)) { ?>
                          <div class="couleurP">
                            <style>
                            input[type=checkbox][name=couleurProduit3] {
                              -webkit-appearance: none;
                              background: url("../couleur/<?=$couleur3?>");
                              border: 1px solid #cacece;
                              box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                              padding: 9px;
                              position: relative;
                            }

                            input[type=checkbox][name=couleurProduit3]:checked {
                              -webkit-appearance: none;
                              border: 2px solid black;
                            }
                            </style>
                          <input type="checkbox" name="couleurProduit3" value="<?="$couleur3";?>"><br></input>
                          </div>
                      <?php } ?>


                      <?php if(!empty($couleur4)) { ?>
                          <div class="couleurP">
                            <style>
                            input[type=checkbox][name=couleurProduit4] {
                              -webkit-appearance: none;
                              background: url("../couleur/<?=$couleur4?>");
                              border: 1px solid #cacece;
                              box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                              padding: 9px;
                              position: relative;
                            }

                            input[type=checkbox][name=couleurProduit4]:checked {
                              -webkit-appearance: none;
                              border: 2px solid black;
                            }
                            </style>
                          <input type="checkbox" name="couleurProduit4" value="<?="$couleur4";?>"><br></input>
                          </div>
                      <?php } ?>

                      <?php if(!empty($couleur5)) { ?>
                          <div class="couleurP">
                            <style>
                            input[type=checkbox][name=couleurProduit5] {
                              -webkit-appearance: none;
                              background: url("../couleur/<?=$couleur5?>");
                              border: 1px solid #cacece;
                              box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                              padding: 9px;
                              position: relative;
                            }

                            input[type=checkbox][name=couleurProduit5]:checked {
                              -webkit-appearance: none;
                              border: 2px solid black;
                            }
                            </style>
                          <input type="checkbox" name="couleurProduit5" value="<?="$couleur5";?>"><br></input>
                          </div>
                      <?php } ?>

                      <?php if(!empty($couleur6)) { ?>
                          <div class="couleurP">
                            <style>
                            input[type=checkbox][name=couleurProduit6] {
                              -webkit-appearance: none;
                              background: url("../couleur/<?=$couleur6?>");
                              border: 1px solid #cacece;
                              box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                              padding: 9px;
                              position: relative;
                            }

                            input[type=checkbox][name=couleurProduit6]:checked {
                              -webkit-appearance: none;
                              border: 2px solid black;
                            }
                            </style>
                          <input type="checkbox" name="couleurProduit6" value="<?="$couleur6";?>"><br></input>
                          </div>
                      <?php } ?>

                      <?php if(!empty($couleur7)) { ?>
                          <div class="couleurP">
                            <style>
                            input[type=checkbox][name=couleurProduit7] {
                              -webkit-appearance: none;
                              background: url("../couleur/<?=$couleu72?>");
                              border: 1px solid #cacece;
                              box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                              padding: 9px;
                              position: relative;
                            }

                            input[type=checkbox][name=couleurProduit7]:checked {
                              -webkit-appearance: none;
                              border: 2px solid black;
                            }
                            </style>
                          <input type="checkbox" name="couleurProduit7" value="<?="$couleur7";?>"><br></input>
                          </div>
                      <?php } ?>

                      <?php if(!empty($couleur8)) { ?>
                          <div class="couleurP">
                            <style>
                            input[type=checkbox][name=couleurProduit8] {
                              -webkit-appearance: none;
                              background: url("../couleur/<?=$couleur8?>");
                              border: 1px solid #cacece;
                              box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                              padding: 9px;
                              position: relative;
                            }

                            input[type=checkbox][name=couleurProduit8]:checked {
                              -webkit-appearance: none;
                              border: 2px solid black;
                            }
                            </style>
                          <input type="checkbox" name="couleurProduit8" value="<?="$couleur8";?>"><br></input>
                          </div>
                      <?php } ?>
                  </div>
                  <div class="detailTaille">
                      <h3>Taille(s)</h3>
                      <form method="post" enctype="multipart/form-data" autocomplete="off">
                        <label for="taille"></label>
                            <select name="taille">
                                <option value="">-- Choisissez une Taille --</option>
                                <?php
                                if(!empty($XS) AND $TailleXS == "XS"){?>
                                <option value="XS">XS</option>
                                <?php }
                                if(!empty($S) AND $TailleS == "S"){?>
                                <option value="S">S</option>
                                <?php }
                                if(!empty($M) AND $TailleM == "M"){?>
                                <option value="M">M</option>
                                <?php }
                                if(!empty($L) AND $TailleL == "L"){?>
                                <option value="L">L</option>
                                <?php }
                                if(!empty($XL) AND $TailleXL == "XL"){?>
                                <option value="XL">XL</option>
                                <?php }
                                if(!empty($XXL) AND $TailleXXL == "XXL"){?>
                                <option value="XXL">XXL</option>
                                <?php }
                                if(!empty($Autre) AND $TailleAutre == "Autre"){?>
                                <option value="Autre">Autre</option>
                              <?php } ?>
                            </select>
                      </form>
                      <button type="submit" class="ajoutPanier" name="button">Ajouter au panier</button>
                  </div>
             </div>


           </form>
       </div>
     </div>




            <div class="detailPrix">
                <p><?=$prix?> €</p>
            </div>

            <?php
            if (isset($_SESSION['user']))
            {
              $emailU = $_SESSION['user'];

              if(isset($_POST['button']))
              {
                if(isset($_POST['couleurProduit']))
                {
                    $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                    $panier -> execute(array(
                        'product_idA' => '0',
                        'user_email' => $emailU,
                        'product_idV' => $idV,
                        'couleurProduit' => $_POST['couleurProduit'],
                        'Taille' => $_POST['taille']
                    ));


                    unset($_POST['button']);
                }
                if(isset($_POST['couleurProduit2']))
                {
                  $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                  $panier -> execute(array(
                      'product_idA' => '0',
                      'user_email' => $emailU,
                      'product_idV' => $idV,
                      'couleurProduit' => $_POST['couleurProduit2'],
                      'Taille' => $_POST['taille']
                  ));
                  unset($_POST['button']);
                }
                if(isset($_POST['couleurProduit3']))
                {
                  $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                  $panier -> execute(array(
                      'product_idA' => '0',
                      'user_email' => $emailU,
                      'product_idV' => $idV,
                      'couleurProduit' => $_POST['couleurProduit3'],
                      'Taille' => $_POST['taille']
                  ));
                  unset($_POST['button']);
                }
                if(isset($_POST['couleurProduit4']))
                {
                  $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                  $panier -> execute(array(
                      'product_idA' => '0',
                      'user_email' => $emailU,
                      'product_idV' => $idV,
                      'couleurProduit' => $_POST['couleurProduit4'],
                      'Taille' => $_POST['taille']
                  ));
                  unset($_POST['button']);
                }
                if(isset($_POST['couleurProduit5']))
                {
                  $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                  $panier -> execute(array(
                      'product_idA' => '0',
                      'user_email' => $emailU,
                      'product_idV' => $idV,
                      'couleurProduit' => $_POST['couleurProduit5'],
                      'Taille' => $_POST['taille']
                  ));
                  unset($_POST['button']);
                }
                if(isset($_POST['couleurProduit6']))
                {
                  $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                  $panier -> execute(array(
                      'product_idA' => '0',
                      'user_email' => $emailU,
                      'product_idV' => $idV,
                      'couleurProduit' => $_POST['couleurProduit6'],
                      'Taille' => $_POST['taille']
                  ));
                  unset($_POST['button']);
                }
                if(isset($_POST['couleurProduit7']))
                {
                  $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                  $panier -> execute(array(
                      'product_idA' => '0',
                      'user_email' => $emailU,
                      'product_idV' => $idV,
                      'couleurProduit' => $_POST['couleurProduit7'],
                      'Taille' => $_POST['taille']
                  ));
                  unset($_POST['button']);
                }
                if(isset($_POST['couleurProduit8']))
                {
                  $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                  $panier -> execute(array(
                      'product_idA' => '0',
                      'user_email' => $emailU,
                      'product_idV' => $idV,
                      'couleurProduit' => $_POST['couleurProduit8'],
                      'Taille' => $_POST['taille']
                  ));
                  unset($_POST['button']);
                }
                  header('Location: vetements.php');
              }





            if(isset($_POST['buttonF'])){
                $panier = $db->prepare('INSERT INTO Favoris(Favo_idA, Favo_idV, user_email)VALUES(:Favo_idA, :Favo_idV, :user_email)');
                $panier -> execute(array(
                    'Favo_idA' => '0',
                    'Favo_idV' => $idV,
                    'user_email' => $emailU
                ));
                unset($_POST['buttonF']);
              }

              if(isset($_POST['buttonSupprFavo'])){
                $panier = $db->exec('DELETE FROM Favoris WHERE Favo_idV = '.$idV);
              }
            }
            else if (!isset($_SESSION['user']) AND (isset($_POST['button'])))
            {
            ?>
              <script>
                alert("<?php echo htmlspecialchars('Veuillez vous connecter pour ajouter un produit au panier !', ENT_QUOTES); ?>")
              </script>
            <?php
            }
        $idV_favo = $db -> prepare('SELECT Favo_idV FROM Favoris WHERE Favo_idV = ?');
        $idV_favo->execute(array($idV));
        $idV_favo = $idV_favo->fetch();
        $idV_favoris = $idV_favo['Favo_idV'];

        if(isset($_SESSION['user']))
        {
        if($idV_favoris == $idV)
        {
          ?>
            <div class="favoris">
                <form method="post" enctype="multipart/form-data">
                  <button type="submit" class="favoris" name="buttonSupprFavo" value="Suppr"><img src="../images/like.png"</button>
                </form>
            </div>
            <?php
        }
        else{
        ?>
          <div class="favoris">
              <form method="post" enctype="multipart/form-data">
                <button type="submit" class="favoris" name="buttonF" value="Ajout"><img src="../images/favoris.png"</button>
              </form>
          </div>
          <?php
        }
      }
?>
            <script type="text/javascript">
            var slideIndex = 1;
            showSlides(slideIndex);

            // Next/previous controls
            function plusSlides(n) {
            showSlides(slideIndex += n);
            }

            // Thumbnail image controls
            function currentSlide(n) {
            showSlides(slideIndex = n);
            }

            function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            }
        </script>
<?php ob_end_flush(); ?>
    </body>
</html>
