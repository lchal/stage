
<?php
    ob_start();
    //Import PHPMailer classes into the global namespace
    //These must be at the top of your script, not inside a function
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    //Load Composer's autoloader
    require('../mail/PHPMailer/Exception.php');
    require('../mail/PHPMailer/PHPMailer.php');
    require('../mail/PHPMailer/SMTP.php');


    if(!empty($_POST['Objet']) && !empty($_POST['Sujet']))
    {
        // Patch XSS

        $Objet = htmlspecialchars($_POST['Objet']);
        $Sujet = htmlspecialchars($_POST['Sujet']);
        $email_user = $_SESSION['user'];



    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->CharSet = 'UTF-8';
        $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'modo.midinet@gmail.com';                     //SMTP username
        $mail->Password   = 'ModoMidinet1!';                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
        $mail->Port       = 587;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

      //  $mail->addAttachment('../images/cacachal.jpg');         //Add attachments

        //Recipients
        $mail->setFrom('modo.midinet@gmail.com', 'Message du client');
        $mail->addAddress('lucas.chalscl@gmail.com');    //Add a recipient
        $mail->addAddress('modo.midinet@gmail.com');

        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = $Objet;
        $mail->Body= "$Sujet <br><br> De :$email_user";


        $mail->send();

        echo 'Message has been sent';


    }
    catch (Exception $e)
    {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
    header("Location:../controleur/contacter.php");
  }
  ob_end_clean();

?>
<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link type="text/css" rel="stylesheet" href="../css/contacter.css">
            <title>Connexion</title>
        </head>
        <body>
    <div id="global">
        <div id="contacter">
            <div class="login-form">
              <h2 id="heading">Nous contacter</h2>
                <form id="formcontact" method="post">
              		<div id="form-card">
              		  <label class="fieldlabels">Objet :</label>
              				<input class="contact" type="text" name="Objet" placeholder="Objet" required autocomplete="off">

              			<label class="fieldlabels">Sujet : </label>
              				<input class="contact" type="text" name="Sujet"  placeholder="Sujet" required autocomplete="off">

              			<button type="submit" name="button" class="action-button">Valider</button>
              		</div>
                </form>
            </div>
        </div>
    </div>
        </body>
</html>
