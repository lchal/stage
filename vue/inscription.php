<!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link type="text/css" rel="stylesheet" href="../css/identification.css">
            <title>Connexion</title>
        </head>
        <body>
    <div id="global">
        <div id="inscription">
            <div class="login-form">
                <?php
                    if(isset($_GET['reg_err']))
                    {
                        $err = htmlspecialchars($_GET['reg_err']);

                        switch($err)
                        {
                            case 'success':
                            ?>
                                <div class="alert alert-success">
                                    <strong>Succès</strong> Veuillez confirmer votre adresse email s'il vous plaît  !
                                </div>
                            <?php
                            break;

                            case 'password':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> Mot de passe différent
                                </div>
                            <?php
                            break;

                            case 'email':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> Email non valide
                                </div>
                            <?php
                            break;

                            case 'email_length':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> Email trop long
                                </div>
                            <?php
                            break;

                            case 'pseudo_length':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> Pseudo trop long
                                </div>
                            <?php
                            break;
                            case 'already':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> Compte deja existant
                                </div>
                            <?php
                            break;
                            case 'ok':
                            ?>
                            <script>
                              alert("<?php echo htmlspecialchars('Vous allez recevoir un mail de confirmation. \nMerci de confirmer votre adresse email.', ENT_QUOTES); ?>")
                            </script>
                            <?php
                            break;
                            case 'validé':
                            ?>
                            <script>
                              alert("<?php echo htmlspecialchars('Votre compte a été validé. \nVous pouvez maintenant vous connecter.', ENT_QUOTES); ?>")
                            </script>
                            <?php
                            break;


                        }
                    }
                    ?>

                <!-- <form action="../controleur/inscription_traitement.php" method="post">
                    <h2 class="text-center">Inscription</h2>
                    <div class="form-group">
                        <input type="text" name="pseudo" class="form-control" placeholder="Pseudo" required="required" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required="required" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Mot de passe" required="required" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password_retype" class="form-control" placeholder="Re-tapez le mot de passe" required="required" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Inscription</button>
                    </div>
                </form> -->

                <h2 id="headingI">Inscription</h2>
                <form id="formInfoI" action="inscription_traitement.php" method="post">
              		<div id="form-card">
              			<label class="fieldlabels">Pseudo: *</label>
              				<input class="inscr" type="text" name="pseudo" placeholder="Pseudo" required autocomplete="off">

              			<label class="fieldlabels">Adresse email: *</label>
              				<input class="inscr" type="email" name="email" placeholder="Email" required autocomplete="off">

                    <label class="fieldlabels">Mot de passe: *</label>
              				<input class="inscr" type="password" name="password" placeholder="Mot de passe" required autocomplete="off">

              			<label class="fieldlabels">Confirmer le mot de passe: *</label>
              				<input class="inscr" type="password" name="password_retype"  placeholder="Veuillez confirmer le mot de passe" required autocomplete="off">

              			<button type="submit" name="button" class="action-button">Valider</button>
              		</div>
                </form>
            </div>
        </div>
    </div>
        </body>
</html>
