<?php
ob_start();
require('../modele/connexionDB.php');
$db = connectBDD();
session_start();

if(!isset($_SESSION['user']) || $_SESSION['admin'] == "0"){
    header('Location: ../controleur/accessoires.php');
    die();
}
else{

    if(isset($_GET['idV'])){

        $idV = htmlentities($db->quote( $_GET['idV']));



        $vetements = $db->prepare("SELECT * FROM vetements WHERE idV =$idV");
        $vetements->execute(array($idV));
        if ($vetements->rowCount() == 1) {
            $vetements = $vetements->fetch();
            $photo = $vetements['photo'];
            $photo2 = $vetements['photo2'];
            $photo3 = $vetements['photo3'];
            $photo4 = $vetements['photo4'];
            $photo5 = $vetements['photo5'];
            $photo6 = $vetements['photo6'];
            $photo7 = $vetements['photo7'];
            $photo8 = $vetements['photo8'];

        }
        else {
            die('Cet article n\'existe pas !');
        }



        $supprV = $db->query("DELETE FROM vetements WHERE idV=$idV");
        $supprFavo = $db->query("DELETE FROM Favoris WHERE Favo_idV=$idV");
        if($supprV){
            unlink("../articles/$photo");
            unlink("../articles/$photo2");
            unlink("../articles/$photo3");
            unlink("../articles/$photo4");
            unlink("../articles/$photo5");
            unlink("../articles/$photo6");
            unlink("../articles/$photo7");
            unlink("../articles/$photo8");
            header("Location: ../controleur/vetements.php");
        }
    }
    if(isset($_GET['idA'])){
        $idA = htmlentities($db->quote( $_GET['idA']));

        $accessoires = $db->prepare("SELECT * FROM accessoires WHERE idA =$idA");
        $accessoires->execute(array($idA));
        if ($accessoires->rowCount() == 1) {
            $accessoires = $accessoires->fetch();
            $photo = $accessoires['photo'];
            $photo2 = $accessoires['photo2'];
            $photo3 = $accessoires['photo3'];
            $photo4 = $accessoires['photo'];
            $photo5 = $accessoires['photo2'];
            $photo6 = $accessoires['photo3'];
            $photo7 = $accessoires['photo'];
            $photo8 = $accessoires['photo2'];

        }
        else {
            die('Cet article n\'existe pas !');
        }


        $supprA = $db->query("DELETE FROM accessoires WHERE idA=$idA");
        $supprFavo = $db->query("DELETE FROM Favoris WHERE Favo_idA=$idA");
        if($supprA){
            unlink("../articles/$photo");
            unlink("../articles/$photo2");
            unlink("../articles/$photo3");
            unlink("../articles/$photo4");
            unlink("../articles/$photo5");
            unlink("../articles/$photo6");
            unlink("../articles/$photo7");
            unlink("../articles/$photo8");
            header("Location: ../controleur/accessoires.php");
        }
    }
}
ob_end_flush();
?>
