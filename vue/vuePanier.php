<?php
require('../modele/connexionDB.php');
include('../modele/fonctionSupprExt.php');
$db = connectBDD();


?>
<link rel="stylesheet" href="../css/panier.css">
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<?php
 if (isset($_SESSION['user'])){
    $user = $_SESSION['user'];

    $panier= $db->query("SELECT * FROM panier where user_email = '$user'");
    $donnes = $panier->fetchAll();

    ?>
    <table>
    <thead>
                <tr>
                    <th scope="col">Produit</th>
                    <th scope="col">Nom du produit</th>
                    <th scope="col">Couleur du produit</th>
                    <th scope="col">Taille</th>
                    <th scope="col">Prix</th>
                    <th scope="col">Retirer article</th>
                </tr>
    </thead>
    <?php
            $total = 0;
        foreach($donnes as $donnes):
            $idP = $donnes['product_idV'];
            $idD = $donnes['product_idA'];
            $id = $donnes['id'];
            ?>
            <tbody>
                <?php
                if($idP !=0)
                {
                  if(isset($idP))
                  {
                    $produitV = $db -> query("SELECT * FROM vetements where idV = '$idP'");
                    $panierV = $produitV->fetchAll();
                    foreach($panierV as $panierV):
                      $photo=$panierV['photo'];
                      $couleurProduitP = $donnes['couleurProduit'];
                      $couleurProduit = SupExt($couleurProduitP);
                      ?>

                      <tr>
                          <td data-label="Account"><?="<img src=\"../articles/$photo\" width=\"100\" alt=\"\"></img>";?></td>
                          <td data-label="Amount"><?= $panierV['nom'];?></td>
                          <td data-label="Amount"><?= ucfirst($couleurProduit);?></td>
                          <td data-label="Amount"><?= $donnes['Taille'];?></td>
                          <td data-label="Period"><?= $panierV['prix'];?>€</td>
                          <td data-label="suppr"><a href="../vue/supprPanier.php?idV=<?=$id?>"><input type="button" value="X"></a>
                      </tr>

                    <?php
                    $total = $total+$panierV['prix'];
                endforeach;
              }
                }
                if($idD !=0){

                    $produitA = $db -> query("SELECT * FROM accessoires where idA = '$idD'");
                    $panierA = $produitA->fetchAll();
                    foreach($panierA as $panierA):
                        $photo=$panierA['photo'];
                        $couleurProduitP = $donnes['couleurProduit'];
                        $couleurProduit = SupExt($couleurProduitP);
                        ?>
                        <tr>
                        <td data-label="Account"><?="<img src=\"../articles/$photo\" width=\"100\" alt=\"\"></img>";?></td>
                        <td data-label="Amount"><?= $panierA['nom'];?></td>
                        <td data-label="Amount"><?= ucfirst($couleurProduit);?></td>
                        <td data-label="Amount"><?= $donnes['Taille'];?></td>
                        <td data-label="Period"><?= $panierA['prix'];?>€</td>
                        <td data-label="suppr"><a href="../vue/supprPanier.php?idA=<?=$id?>"><input type="button" value="X"></a></td>
                    </tr>
                    <?php
                    $total = $total+$panierA['prix'];
                endforeach;
                }

            ?>

                </tbody>
            <?php


        endforeach;
        if ($total == 0)
        {
            $total = 0;
        }
        else
        {

          $pntFidelite_user= $db->query("SELECT * FROM utilisateurs where email = '$user'");
          $donnes_user = $pntFidelite_user->fetchAll();

          foreach($donnes_user as $donnes_user):
            $pntFidelite = $donnes_user['pntFidelite'];
          endforeach;
          if($pntFidelite < 200){
            $total = $total*1.05+0.7+4.5;
            $_SESSION['total']= $total;
            ?>
            <div class="paie">
                <form name="click" action="valideCom.php">
                    <input type="image" src="../images/commander.png" name="submit" alt="">
                </form>
                <h2><?=number_format($total,2)?>€ TTC</h2>
                <h4 class="Protection">Frais de Protection acheteurs</h4>
                <div class="frais">0,70 € + 5% du prix total</br>
                + 4,5€ de livraison</div>
                <div class="vente">
                    Paiement: Paypal
                </div>
            </div>
          <?php
         }
           else {
             $total = $total*0.8;
             $total = $total*1.05+0.7+4.5;
             $_SESSION['total']= $total;
             ?>
             <div class="paie">
                 <form name="click" action="valideCom.php">
                     <input type="image" src="../images/commander.png" name="submit" alt="">
                 </form>
                 <h2><?=number_format($total,2)?>€ TTC</h2>
                 <h6>Vous avez une remise de 20% sur votre panier grace aux points de fidelite</h6>
                 <h4 class="Protection">Frais de Protection acheteurs</h4>
                 <div class="frais">0,70 € + 5% du prix total</br>
                 + 4,5€ de livraison</div>
                 <div class="vente">
                     Paiement: Paypal
                 </div>
             </div>
             <?php
           }
      }?>
    </table>
    <?php


}

else {
    echo "Connecte toi pour avoir ton panier";
} ?>
