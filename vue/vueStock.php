<?php
if(!isset($_SESSION['user']) || $_SESSION['admin'] == "0")
{
  header('Location: ../controleur/accessoires.php');
  die();
}

  $vetement = $db -> prepare('SELECT * FROM vetements');
  $accessoire = $db -> prepare('SELECT * FROM accessoires');

  $executeisOk = $vetement -> execute();
  $executeestOk = $accessoire -> execute();

  $vetements = $vetement->fetchAll();
  $accessoires = $accessoire->fetchAll();
?>

<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="../css/stock.css" rel="stylesheet">
  </head>
  <div class= "cadreP">
    <h1>Aperçu de nos produits:</h1>
    <?php
    foreach ($vetements as $vetements):
      $photo=$vetements['photo'];
      $afficher =$vetements['afficher'];
      $nbr=$vetements['nbr'];
      $idV=$vetements['idV'];
      $XS=$vetements['XS'];
      $S=$vetements['S'];
      $M=$vetements['M'];
      $L=$vetements['L'];
      $XL=$vetements['XL'];
      $XXL=$vetements['XXL'];
      $Autre=$vetements['Autre'];

    ?>
      <div class="container">
        <div class="card">
          <div class="card-header">
            <?php echo "<img src=\"../articles/$photo\" alt=\"rover\" />";?>
          </div>


          <div class="card-body">
            <span class="tag tag-teal"><?= $vetements['nom'] ?> </span>
            <h4>
            Prix : <?= $vetements['prix'] ?>€
            </h4>

            <?php
            if($afficher == 0)
            {
              ?>
                <div class="afficher">
                    <form action="../controleur/stock_traitement.php?idV=<?=$idV;?>&afficher=<?=$afficher;?>" method="post" enctype="multipart/form-data">
                      <button type="submit" class="afficher" name="pasAfficher" value="0"><img src="../images/nonCocher.png"></button>
                    </form>
                </div>
                <?php
            }
            else
            {
            ?>
              <div class="afficher">
                  <form action="../controleur/stock_traitement.php?idV=<?=$idV;?>&afficher=<?=$afficher;?>" method="post" enctype="multipart/form-data">
                    <button type="submit" class="afficher" name="estAfficher" value="1"><img src="../images/cocher.png"></button>
                  </form>
              </div>
              <?php
            }
            ?>

            <div class="nbr">
                <form action="../controleur/nbr_traitement.php?idV=<?=$idV;?>" method="post" enctype="multipart/form-data">
                  <input type="number" class="nbr" name="stock" value="<?=$nbr;?>"></input>
                  <button type="submit" name="modif" class="btn btn-primary btn-block">Modifier</button>
                </form>
            </div>

            <div id="tailleV">
              <form action="../controleur/taille_traitement.php?idV=<?=$idV;?>" method="post" enctype="multipart/form-data">

                <?php
                if ($XS==1)
                {
                  echo"<label class=\"taille\" for=\"XS\">XS</label>";
                  echo"<input id=\"XS\" name=\"XS\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"XS\">XS</label>";
                  echo"<input id=\"XS\" name=\"XS\" type=\"checkbox\">";

                }

                if ($S==1)
                {
                  echo"<label class=\"taille\" for=\"S\">S</label>";
                  echo"<input id=\"S\" name=\"S\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"S\">S</label>";
                  echo"<input id=\"S\" name=\"S\" type=\"checkbox\">";

                }

                if ($M==1)
                {
                  echo"<label class=\"taille\" for=\"M\">M</label>";
                  echo"<input id=\"M\" name=\"M\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"M\">M</label>";
                  echo"<input id=\"M\" name=\"M\" type=\"checkbox\">";

                }

                if ($L==1)
                {
                  echo"<label class=\"taille\" for=\"L\">L</label>";
                  echo"<input id=\"L\" name=\"L\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"L\">L</label>";
                  echo"<input id=\"L\" name=\"L\" type=\"checkbox\">";

                }

                if ($XL==1)
                {
                  echo"<label class=\"taille\" for=\"XL\">XL</label>";
                  echo"<input id=\"XL\" name=\"XL\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"XL\">XL</label>";
                  echo"<input id=\"XL\" name=\"XL\" type=\"checkbox\">";

                }

                if ($XXL==1)
                {
                  echo"<label class=\"taille\" for=\"XXL\">XXL</label>";
                  echo"<input id=\"XXL\" name=\"XXL\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"XXL\">XXL</label>";
                  echo"<input id=\"XXL\" name=\"XXL\" type=\"checkbox\">";

                }

                if ($Autre==1)
                {
                  echo"<label class=\"taille\" for=\"Autre\">Autre</label>";
                  echo"<input id=\"Autre\" name=\"Autre\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"Autre\">Autre</label>";
                  echo"<input id=\"Autre\" name=\"Autre\" type=\"checkbox\">";

                }
                ?>
                <button type="submit" name="taille" class="btn btn-primary btn-block">TAILLE</button>
              </form>
            </div>

            <div class="suppr">
              <form action="../vue/suppr.php?idV=<?=$idV;?>" method="post" enctype="multipart/form-data">
                <button type="submit" name="suppr" class="btn btn-primary btn-block">Supprimer l'article</button>
              </form>
            </div>

          </div>
        </div>
      </div>
    <?php
    endforeach
    ?>

    <?php
    foreach ($accessoires as $accessoires):
      $photo=$accessoires['photo'];
      $afficher =$accessoires['afficher'];
      $nbr=$accessoires['nbr'];
      $idA=$accessoires['idA'];


    ?>
      <div class="container">
        <div class="card">
          <div class="card-header">
            <?php echo "<img src=\"../articles/$photo\" alt=\"rover\" />";?>
          </div>


          <div class="card-body">
            <span class="tag tag-teal"><?= $accessoires['nom'] ?> </span>
            <h4>
            Prix : <?= $accessoires['prix'] ?>€
            </h4>

            <?php
            if($afficher == 0)
            {
              ?>
                <div class="afficher">
                    <form action="../controleur/stock_traitement.php?idA=<?=$idA;?>&afficher=<?=$afficher;?>" method="post" enctype="multipart/form-data">
                      <button type="submit" class="afficher" name="pasAfficher" value="0"><img src="../images/nonCocher.png"></button>
                    </form>
                </div>
                <?php
            }
            else
            {
            ?>
              <div class="afficher">
                  <form action="../controleur/stock_traitement.php?idA=<?=$idA;?>&afficher=<?=$afficher;?>" method="post" enctype="multipart/form-data">
                    <button type="submit" class="afficher" name="estAfficher" value="1"><img src="../images/cocher.png"></button>
                  </form>
              </div>
              <?php
            }
            ?>

            <div class="nbr">
                <form action="../controleur/nbr_traitement.php?idA=<?=$idA;?>" method="post" enctype="multipart/form-data">
                  <input type="number" class="nbr" name="stock" value="<?=$nbr;?>"></input>
                  <button type="submit" name="modif" class="btn btn-primary btn-block">Modifier</button>
                </form>
            </div>

            <div id="tailleA">
              <form action="../controleur/taille_traitement.php?idA=<?=$idA;?>" method="post" enctype="multipart/form-data">

                <?php
                if ($XS==1)
                {
                  echo"<label class=\"taille\" for=\"XS\">XS</label>";
                  echo"<input id=\"XS\" name=\"XS\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"XS\">XS</label>";
                  echo"<input id=\"XS\" name=\"XS\" type=\"checkbox\">";

                }

                if ($S==1)
                {
                  echo"<label class=\"taille\" for=\"S\">S</label>";
                  echo"<input id=\"S\" name=\"S\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"S\">S</label>";
                  echo"<input id=\"S\" name=\"S\" type=\"checkbox\">";

                }

                if ($M==1)
                {
                  echo"<label class=\"taille\" for=\"M\">M</label>";
                  echo"<input id=\"M\" name=\"M\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"M\">M</label>";
                  echo"<input id=\"M\" name=\"M\" type=\"checkbox\">";

                }

                if ($L==1)
                {
                  echo"<label class=\"taille\" for=\"L\">L</label>";
                  echo"<input id=\"L\" name=\"L\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"L\">L</label>";
                  echo"<input id=\"L\" name=\"L\" type=\"checkbox\">";

                }

                if ($XL==1)
                {
                  echo"<label class=\"taille\" for=\"XL\">XL</label>";
                  echo"<input id=\"XL\" name=\"XL\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"XL\">XL</label>";
                  echo"<input id=\"XL\" name=\"XL\" type=\"checkbox\">";

                }

                if ($XXL==1)
                {
                  echo"<label class=\"taille\" for=\"XXL\">XXL</label>";
                  echo"<input id=\"XXL\" name=\"XXL\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"XXL\">XXL</label>";
                  echo"<input id=\"XXL\" name=\"XXL\" type=\"checkbox\">";

                }

                if ($Autre==1)
                {
                  echo"<label class=\"taille\" for=\"Autre\">Autre</label>";
                  echo"<input id=\"Autre\" name=\"Autre\" type=\"checkbox\" checked>";

                }
                else
                {
                  echo"<label class=\"taille\" for=\"Autre\">Autre</label>";
                  echo"<input id=\"Autre\" name=\"Autre\" type=\"checkbox\">";

                }
                ?>
                <button type="submit" name="taille" class="btn btn-primary btn-block">TAILLE</button>
              </form>
            </div>

            <div class="suppr">
              <form action="../vue/suppr.php?idA=<?=$idA;?>" method="post" enctype="multipart/form-data">
                <button type="submit" name="suppr" class="btn btn-primary btn-block">Supprimer l'article</button>
              </form>
            </div>

          </div>
        </div>
      </div>
    <?php
    endforeach
    ?>
  </div>
</html>
