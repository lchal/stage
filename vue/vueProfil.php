<?php
require('../modele/connexionDB.php');
$db = connectBDD();
?>
<html lang="fr">
  <head>
    <title>Espace membre</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="../css/profil.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  </head>

  <body>
    <div class="profil-gauche">
      <div class="sidebar">
        <ul id="liste-gauche">
          <li>
            <?php
            if(isset($_SESSION['user']))
            {
              echo "<a href=\"/stage/controleur/infoPerso.php\">Informations personnelles</a>";
            }
            ?>
          </li>
          <li>
            <a href="/stage/controleur/modifMdp.php">Modifier mon mot de passe</a>
          </li>
          <li>
            <a href="/stage/controleur/commandeUtilisateur.php">Mes commandes</a>
          </li>
          <li>
            <a href="/stage/controleur/contacter.php">Nous contacter</a>
          </li>
          <li>
            <a href="/stage/controleur/favoris.php">Mes favoris</a>
          </li>
          <?php
          if(isset($_SESSION['user']) AND $_SESSION['admin'] == "1")
          {
          ?>
            <li>
              <a href="/stage/controleur/listeCommande.php">Liste des commandes des clients</a>
            </li>
          <?php
          }

          if(isset($_SESSION['user']) AND $_SESSION['admin'] == "1")
          {
          ?>
            <li>
              <a href="/stage/controleur/stock.php">Liste des stocks</a>
            </li>
          <?php
          }
          ?>
        </ul>
      </div>
    </div>
  </body>
</html>
