<?php
include('../modele/fonctionAjoutVetement.php');
ob_start();

ajoutVetement();

if(!isset($_SESSION['user']) || $_SESSION['admin'] == "0"){
    header('Location: ../controleur/vetements.php');
    die();
}

?>


<link rel="stylesheet" href="../css/vueAjoutVetements.css">
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11 col-sm-10 col-md-10 col-lg-6 col-xl-5 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <h2 id="heading">Ajout d'un nouveau vêtement</h2>
                <!-- <p>Fill all form field to go to next step</p> -->
                <form method="post" enctype="multipart/form-data"  id="msform">
                    <!-- progressbar -->
                    <ul id="progressbar">
                        <li class="active" id="account"><strong>Caractéristiques</strong></li>
                        <li id="personal"><strong>Sélection des images</strong></li>
                        <li id="payment"><strong>Sélection des couleurs</strong></li>
                        <!-- <li id="confirm"><strong>Finish</strong></li> -->
                    </ul>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    </div> <br> <!-- fieldsets -->
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Caractéristiques du produit:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Étape 1 - 3</h2>
                                </div>
                            </div>
                            <label class="fieldlabels">Nom: *</label>
                              <input class="caractV" type="text" name="nom" placeholder="Nom" required>

                            <label class="fieldlabels">Quantité: *</label>
                              <input class="caractV" type="number" name="nbr" placeholder="Exemple: 13" min="1" required>

                            <label class="fieldlabels">Description: *</label>
                              <input class="caractV" type="text" name="descr" placeholder="Description" required>

                            <label class="fieldlabels">Prix: *</label>
                              <input class="caractV" type="text" name="prix" placeholder="Prix" required>

                              <label for="type">Type de vetements:</label>
                                  <select name="type">
                                      <option value="">-- Choisissez une option --</option>
                                      <option value="Haut">Haut</option>
                                      <option value="Bas">Bas</option>
                                      <option value="Autres">Autres</option>
                                  </select>

                        </div>
                        <input type="button" name="next" class="next action-button" value="Suivant" />
                    </fieldset>

                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Séléction des images du produit:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Étape 2 - 3</h2>
                                </div>
                            </div>
                            <input type="file" class="custom-file-inputP" name="photo" placeholder="photo"; required>
                            <input type="file" class="custom-file-inputP" name="photo2" placeholder="photo">
                            <input type="file" class="custom-file-inputP" name="photo3" placeholder="photo">
                            <input type="file" class="custom-file-inputP" name="photo4" placeholder="photo">
                            <input type="file" class="custom-file-inputP" name="photo5" placeholder="photo">
                            <input type="file" class="custom-file-inputP" name="photo6" placeholder="photo">
                            <input type="file" class="custom-file-inputP" name="photo7" placeholder="photo">
                            <input type="file" class="custom-file-inputP" name="photo8" placeholder="photo">
                          </div>
                        <input type="button" name="next" class="next action-button" value="Suivant" />
                        <input type="button" name="previous" class="previous action-button-previous" value="Précédent" />
                    </fieldset>
                    <fieldset>
                        <div class="form-card">
                            <div class="row">
                                <div class="col-7">
                                    <h2 class="fs-title">Séléction des couleurs (images) et taille:</h2>
                                </div>
                                <div class="col-5">
                                    <h2 class="steps">Étape 3 - 3</h2>
                                </div>
                            </div>
                            <input type="file" class="custom-file-inputC" name="couleur" placeholder="couleur" required>
                            <input type="file" class="custom-file-inputC" name="couleur2" placeholder="couleur">
                            <input type="file" class="custom-file-inputC" name="couleur3" placeholder="couleur">
                            <input type="file" class="custom-file-inputC" name="couleur4" placeholder="couleur">
                            <input type="file" class="custom-file-inputC" name="couleur5" placeholder="couleur">
                            <input type="file" class="custom-file-inputC" name="couleur6" placeholder="couleur">
                            <input type="file" class="custom-file-inputC" name="couleur7" placeholder="couleur">
                            <input type="file" class="custom-file-inputC" name="couleur8" placeholder="couleur">

                            <input id="XS" name="XS" type="checkbox">
                            <label class="taille" for="XS">XS</label>
                            <input id="S" name="S" type="checkbox">
                            <label class="taille" for="S">S</label>
                            <input id="M" name="M" type="checkbox">
                            <label class="taille" for="M">M</label>
                            <input id="L" name="L" type="checkbox">
                            <label class="taille" for="L">L</label>
                            <input id="XL" name="XL" type="checkbox">
                            <label class="taille" for="XL">XL</label>
                            <input id="XXL" name="XXL" type="checkbox">
                            <label class="taille" for="XXL">XXL</label>
                            <input id="Autre" name="Autre" type="checkbox">
                            <label class="taille" for="Autre">Autre</label>
                        </div>
                        <!-- <input type="button" name="next" class="next action-button" value="Submit" /> -->
                        <input type="submit" name="buttonAjout" class="next action-button" value="Ajouter">
                        <!-- <a href="vetements.php"><input type="button" class="next action-button" value="Annuler"></a> -->
                        <input type="button" name="previous" class="previous action-button-previous" value="Précédent" />
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>

<script type="text/javascript" src="../scriptAjout.js"></script>
