<html>
<head>
  <link type="text/css" rel="stylesheet" href="../css/vueModifMdp.css">
</head>
  <body>
  <!-- <div class="login-form">
    <form action="modifMdp.php" method="post">
        <h2 class="text-center">Modifier mon mot de passe</h2>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Modifier mon mot de passe" required="required" autocomplete="off">
        </div>
        <div class="form-group">
            <input type="password" name="password_retype" class="form-control" placeholder="Confirmer nouveau le mot de passe" required="required" autocomplete="off">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Confirmer</button>
        </div>
    </form>
  </div>
   -->

    <h2 id="heading">Modifier mon mot de passe</h2>
    <form id="formInfo" action="modifMdp.php" method="post">
  		<div id="form-card">
  			<label class="fieldlabels">Nouveau mot de passe: *</label>
  				<input class="pwd" type="password" name="password" required autocomplete="off">

  			<label class="fieldlabels">Confirmer le nouveau mot de passe: *</label>
  				<input class="pwd" type="password" name="password_retype" required autocomplete="off">

  			<button type="submit" name="button" class="action-button">Valider</button>
  		</div>
    </form>
  </body>
</html>
