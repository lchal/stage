<head>
  <link type="text/css" rel="stylesheet" href="../css/vueDetailCommande.css">
</head>
<?php
if(isset($_SESSION['user'])){
  $email = $_SESSION['user'];

  $infoU= $db->prepare("SELECT * FROM info_commandes WHERE email_user = ?");
  $executeisOk = $infoU -> execute(array($email));
  $infoU = $infoU ->fetchAll();

  ?>

  <table>
    <thead>
      <tr>
        <th scope="col">Nom</th>
        <th scope="col">Prénom</th>
        <th scope="col">Numéro de Téléphone</th>
        <th scope="col">Adresse de livraison</th>
        <th scope="col">Code Postal</th>
        <th scope="col">Numéro de commande</th>
        <th scope="col">Situation</th>
      </tr>
    </thead>

    <?php
      foreach ($infoU as $infoU):
        $num_com = $infoU['num_com'];
        $com_Valider = $infoU['commande_valider'];
    ?>
    <form method="post">
        <tbody>
          <tr>
              <td><?= $infoU['prenom'];?></td>
              <td><?= $infoU['nom'];?></td>
              <td><?= $infoU['tel'];?></td>
              <td><?= $infoU['adresse'];?></td>
              <td><?= $infoU['CP'];?></td>
              <td><a href="detailCommande.php?num_com=<?=$num_com?>"><?= $infoU['num_com'];?></a></td>
              <?php
              if($com_Valider == 1)
              {
                echo "<td>Commande traitée</td>";
              }
              else
              {
                echo "<td>En cours de traitement</td>";
              }
              ?>

          </tr>
        </tbody>
      </form>
    <?php
  endforeach;
  }
  ?>
  </table>
