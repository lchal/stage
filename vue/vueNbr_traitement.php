<?php
if(!isset($_SESSION['user']) || $_SESSION['admin'] == "0")
{
  header('Location: ../controleur/stock.php');
  die();
}

if(!empty($_POST['stock']))
{
  $nbr = $_POST['stock'];

  if(isset($_GET['idA']))
  {
    $idA = $_GET['idA'];
    $stockA = $db -> prepare("UPDATE accessoires SET nbr = ? WHERE idA = ?");
    $stockA -> execute(array($nbr, $idA));

    header('Location: stock.php');die();
  }
}

if(!empty($_POST['stock']))
{
  $nbr = $_POST['stock'];

  if(isset($_GET['idV']))
  {
    $idV = $_GET['idV'];
    $stockV = $db -> prepare("UPDATE vetements SET nbr = ? WHERE idV = ?");
    $stockV -> execute(array($nbr, $idV));

    header('Location: stock.php');die();
  }
}
