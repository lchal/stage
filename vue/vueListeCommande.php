<head>
  <link type="text/css" rel="stylesheet" href="../css/vueListeCommande.css">
</head>
<table>
  <thead>
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Prénom</th>
      <th scope="col">Numéro de Téléphone</th>
      <th scope="col">Adresse de livraison</th>
      <th scope="col">Code Postal</th>
      <th scope="col">Numéro de commande</th>
      <th scope="col">Valider commande</th>
      <th scope="col">Supprimer commande</th>
    </tr>
  </thead>

  <?php

  if(!isset($_SESSION['user']) || $_SESSION['admin'] == "0")
  {
    header('Location: ../controleur/accessoires.php');
    die();
  }

    $infoU= $db->query("SELECT * FROM info_commandes");
    $infoU = $infoU->fetchAll();
    foreach ($infoU as $infoU):
      $num_com = $infoU['num_com'];
      $com_Valider = $infoU['commande_valider'];
      $email = $infoU['email_user'];
  ?>
  <form method="post">
      <tbody>
        <tr>
            <td><?= $infoU['prenom'];?></td>
            <td><?= $infoU['nom'];?></td>
            <td><?= $infoU['tel'];?></td>
            <td><?= $infoU['adresse'];?></td>
            <td><?= $infoU['CP'];?></td>
            <td><a href="detailCommande.php?num_com=<?=$num_com?>"><?= $infoU['num_com'];?></a></td>
            <?php
            if($infoU['commande_valider'] != 1)
            {
            ?>
            <td><a href="../controleur/listeCommande_traitement.php?num_com=<?=$num_com?>&valider=<?=$com_Valider?>&email=<?=$email?>"><img src="../images/non_valider.png"/></a></td>
            <?php
            }
            else
            {
            ?>
            <td><a href="../controleur/listeCommande_traitement.php?num_com=<?=$num_com?>&valider=<?=$com_Valider?>&email=<?=$email?>"><img src="../images/valider.png"/></a></td>
            <?php
            }
            ?>
            <td><a href="../controleur/supprCommandeAdmin.php?num_com=<?=$num_com?>"><img src="../images/rejeter.png"/></a></td>
        </tr>
      </tbody>
    </form>
  <?php
endforeach;

  ?>
</table>
