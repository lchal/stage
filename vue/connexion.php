<!DOCTYPE html>
    <html lang="en">
    <?php

    ?>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
            <link rel="stylesheet" href="../css/identification.css">
            <title>Connexion</title>
        </head>
        <body>
            <div id="global">
        <div id="connexion">
            <div class="login-form">
                <?php
                    if(isset($_GET['login_err']))
                    {
                        $err = htmlspecialchars($_GET['login_err']);

                        switch($err)
                        {
                            case 'password':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> mot de passe incorrect
                                </div>
                            <?php
                            break;

                            case 'email':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> email incorrect
                                </div>
                            <?php
                            break;

                            case 'already':
                            ?>
                                <div class="alert alert-danger">
                                    <strong>Erreur</strong> compte non existant
                                </div>
                            <?php
                            break;
                        }
                    }
                    ?>
                <h2 id="headingC">Connexion</h2>
                <form id="formInfoC" action="connexion_traitement.php" method="post">
                  <div id="form-card">
                    <label class="fieldlabels">Adresse email: *</label>
                      <input class="connex" type="email" name="email" placeholder="Email" required autocomplete="off">

                    <label class="fieldlabels">Mot de passe: *</label>
                      <input class="connex" type="password" name="password" placeholder="Mot de passe" required autocomplete="off">

                    <button type="submit" name="button" class="action-button">Valider</button>
                  </div>
                </form>

            </div>
        </div>
    </div>

        </body>
</html>
