<?php
ob_start();
if(!isset($_SESSION['user']) || $_SESSION['admin'] == "0")
{
  header('Location: ../controleur/accessoires.php');
  die();
}

if(isset($_GET['afficher']) AND isset($_GET['idA']))
{
  $idA = $_GET['idA'];
  $afficher = $_GET['afficher'];

  if($afficher == '0')
  {
    $afficherA = $db -> prepare("UPDATE accessoires SET afficher = ? WHERE idA = ?");
    $afficherA -> execute(array('1', $idA));

    header('Location: ../controleur/stock.php');die();
  }
  else if($afficher == '1')
  {
    $pasAfficherA = $db -> prepare("UPDATE accessoires SET afficher = ? WHERE idA = ?");
    $pasAfficherA -> execute(array('0', $idA));

    header('Location: ../controleur/stock.php');die();
  }
}

if(isset($_GET['afficher']) AND isset($_GET['idV']))
{
  $idV = $_GET['idV'];
  $afficher = $_GET['afficher'];

  if($afficher == '0')
  {
    $afficherV = $db -> prepare("UPDATE vetements SET afficher = ? WHERE idV = ?");
    $afficherV -> execute(array('1', $idV));

    header('Location: stock.php');die();
  }
  else if($afficher == '1')
  {
    $pasAfficherV = $db -> prepare("UPDATE vetements SET afficher = ? WHERE idV = ?");
    $pasAfficherV -> execute(array('0', $idV));

    header('Location: stock.php');die();
  }
}
ob_end_flush();
?>
