<?php
require('../modele/connexionDB.php');
$db = connectBDD();
include('../modele/fonctionSupprExt.php');

if(isset($_SESSION['total']) && isset($_SESSION['user']) && isset($_SESSION['numCom']))
{
  $get_NumCom = $_SESSION['numCom'];
  $get_email = $_SESSION['user'];
  $get_total = $_SESSION['total'];
  $panier= $db->query("SELECT * FROM panier WHERE user_email = '$get_email'");
  $donnes = $panier->fetchAll();

?>
<head>
  <link rel="stylesheet" href="../css/vuePaiement.css">
</head>

<h2>Récapitulatif de la commande</h2>
<table id="produit">
<thead>
            <tr>
                <th scope="col">Produit</th>
                <th scope="col">Nom du produit</th>
                <th scope="col">Couleur du produit</th>
                <th scope="col">Taille</th>
                <th scope="col">Prix</th>
            </tr>
</thead>
<?php
    foreach($donnes as $donnes):
        $idP = $donnes['product_idV'];
        $idD = $donnes['product_idA'];
        $id = $donnes['id'];
        ?>
        <tbody>
            <?php
            if($idP !=0)
            {
              if(isset($idP))
              {
                $produitV = $db -> query("SELECT * FROM vetements where idV = '$idP'");
                $panierV = $produitV->fetchAll();
                foreach($panierV as $panierV):
                  $photo=$panierV['photo'];
                  $couleurProduitP = $donnes['couleurProduit'];
                  $couleurProduit = SupExt($couleurProduitP);
                  ?>

                  <tr>
                      <td data-label="Account"><?="<img src=\"../articles/$photo\" width=\"100\" alt=\"\"></img>";?></td>
                      <td data-label="Amount"><?= $panierV['nom'];?></td>
                      <td data-label="Amount"><?= ucfirst($couleurProduit);?></td>
                      <td data-label="Amount"><?= $donnes['Taille'];?></td>
                      <td data-label="Period"><?= $panierV['prix'];?>€</td>
                  </tr>

                <?php
            endforeach;
          }
            }
            if($idD !=0){

                $produitA = $db -> query("SELECT * FROM accessoires where idA = '$idD'");
                $panierA = $produitA->fetchAll();
                foreach($panierA as $panierA):
                    $photo=$panierA['photo'];
                    $couleurProduitP = $donnes['couleurProduit'];
                    $couleurProduit = SupExt($couleurProduitP);
                    ?>
                    <tr>
                    <td data-label="Account"><?="<img src=\"../articles/$photo\" width=\"100\" alt=\"\"></img>";?></td>
                    <td data-label="Amount"><?= $panierA['nom'];?></td>
                    <td data-label="Amount"><?= ucfirst($couleurProduit);?></td>
                    <td data-label="Amount"><?= $donnes['Taille'];?></td>
                    <td data-label="Period"><?= $panierA['prix'];?>€</td>
                </tr>
                <?php
            endforeach;
            }

        ?>

            </tbody>
        <?php
    endforeach;
  ?>
</table>
<?php
$infoU= $db->prepare("SELECT * FROM info_commandes WHERE num_com = ?");
$executeisOk = $infoU -> execute(array($get_NumCom));
$infoU = $infoU ->fetchAll();


foreach($infoU as $infoU):
?>

<table id="client">
  <thead>
    <tr>
      <th scope="col">Nom</th>
      <td><?= $infoU['nom'];?></td>
    </tr>

    <tr>
      <th scope="col">Prénom</th>
      <td><?= $infoU['prenom'];?></td>
    </tr>

    <tr>
      <th scope="col">Numéro de Téléphone</th>
      <td><?= $infoU['tel'];?></td>
    </tr>

    <tr>
      <th scope="col">Adresse de livraison</th>
      <td><?= $infoU['adresse'];?></td>
    </tr>

    <tr>
      <th scope="col">Code Postal</th>
      <td><?= $infoU['CP'];?></td>
    </tr>

    <tr>
      <th scope="col">Numéro de commande</th>
      <td><?= $get_NumCom;?></a></td>
    </tr>
  </thead>
</thead>
</table>

<?php
endforeach;
}

$pntFidelite_user= $db->query("SELECT * FROM utilisateurs where email = '$get_email'");
$donnes_user = $pntFidelite_user->fetchAll();

foreach($donnes_user as $donnes_user):
  $pntFidelite = $donnes_user['pntFidelite'];
endforeach;
if($pntFidelite < 200){
 $get_total  = $get_total *1.05+0.7+4.5;
 ?>
 <div class="paie">
   <form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
     <input type="hidden" name="cmd" value="_xclick">
     <input type="hidden" name="business" value="c.farguette@gmail.com">
     <input type="hidden" name="currency_code" value="EUR">
     <input type="hidden" name="item_name" value="Teddy Bear">
     <input type="hidden" name="amount" value="<?=$get_total?>">
     <input type="image" src="../images/commander.png" name="submit" alt="Make payments with Paypal - it's fast, free and secure!">
   </form>
     <h2><?=number_format($get_total ,2)?>€ TTC</h2>
     <h4 class="Protection">Frais de Protection acheteurs</h4>
     <div class="frais">0,70 € + 5% du prix total</br>
     + 4,5€ de livraison</div>
     <div class="vente">
         Paiement: Paypal
     </div>
 </div>
 <?php
}
 else {
  $get_total = $get_total *0.8;
  $get_total  = $get_total *1.05+0.7+4.5;
  ?>
  <div class="paie">
    <form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
      <input type="hidden" name="cmd" value="_xclick">
      <input type="hidden" name="business" value="c.farguette@gmail.com">
      <input type="hidden" name="currency_code" value="EUR">
      <input type="hidden" name="item_name" value="Teddy Bear">
      <input type="hidden" name="amount" value="<?=$get_total?>">
      <input type="image" src="../images/commander.png" name="submit" alt="Make payments with Paypal - it's fast, free and secure!">
    </form>
      <h2><?=number_format($get_total ,2)?>€ TTC</h2>
      <h6>Vous avez une remise de 20% sur votre panier grace aux points de fidelite</h6>
      <h4 class="Protection">Frais de Protection acheteurs</h4>
      <div class="frais">0,70 € + 5% du prix total</br>
      + 4,5€ de livraison</div>
      <div class="vente">
          Paiement: Paypal
      </div>
  </div>
<?php } ?>
