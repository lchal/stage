<?php
ob_start();
require('../modele/connexionDB.php');
$db = connectBDD();

if(isset($_GET['idA']) AND !empty($_GET['idA'])) {
    $get_idA = htmlspecialchars($_GET['idA']);
    $accessoires = $db->prepare('SELECT * FROM accessoires WHERE idA = ?');
    $accessoires->execute(array($get_idA));
    $utilisateurs = $db->prepare('SELECT * FROM utilisateurs');
    $utilisateurs = $utilisateurs->fetch();
    if ($accessoires->rowCount() == 1) {
        $accessoires = $accessoires->fetch();
        $nom = $accessoires['nom'];
        $descr = $accessoires['descr'];
        $nbr = $accessoires['nbr'];
        $couleur = $accessoires['couleur'];
        $couleur2 = $accessoires['couleur2'];
        $couleur3 = $accessoires['couleur3'];
        $couleur4 = $accessoires['couleur4'];
        $couleur5 = $accessoires['couleur5'];
        $couleur6 = $accessoires['couleur6'];
        $couleur7 = $accessoires['couleur7'];
        $couleur8 = $accessoires['couleur8'];
        $prix = $accessoires['prix'];
        $photo = $accessoires['photo'];
        $photo2 = $accessoires['photo2'];
        $photo3 = $accessoires['photo3'];
        $idA = $accessoires['idA'];
    }
    else {
        die('Cet article n\'existe pas !');
    }
}


if(!isset($_SESSION['panier']))
    $_SESSION['panier'] = [];
?>


<!DOCTYPE html>
<html>
    <head>
    <title>Détail Accessoires</title>
    <meta charset="utf-8">
    <link type="text/css" rel="stylesheet" href="../css/vueDetail.css">


    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    </head>
    <body>
        <!-- Slideshow container -->
        <div class="slideshow-container">

        <!-- Full-width images with number and caption text -->
        <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo\" alt=\"rover\" />";?>
        </div>


        <?php if(!empty($photo2)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo2\" alt=\"rover\" />";?>
            </div>
        <?php } ?>


        <?php if(!empty($photo3)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo3\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <?php if(!empty($photo4)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo4\" alt=\"rover\" />";?>
            </div>
        <?php } ?>


        <?php if(!empty($photo5)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo5\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <?php if(!empty($photo6)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo6\" alt=\"rover\" />";?>
            </div>
        <?php } ?>


        <?php if(!empty($photo7)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo7\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <?php if(!empty($photo8)) { ?>
            <div class="mySlides fade">
            <?="<img class=\"detail\" src=\"../articles/$photo8\" alt=\"rover\" />";?>
            </div>
        <?php } ?>

        <!-- Next and previous buttons -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>

        <div class="infos">
            <div class="detailNom">
                <h1><?=$nom?></h1>
            </div>

            <div class="container-Detail">
                <div class="detailDescr">
                    <h3>Description </h3>
                    <?=$descr?>
                </div>

                <div class="ajoutPanier">
                    <form method="post" enctype="multipart/form-data">
                <div class="detailCouleur">
                    <h3>Couleur(s)</h3>
                    <div class="couleurP">
                      <style>
                      input[type=checkbox][name=couleurProduit] {
                        -webkit-appearance: none;
                        background: url("../couleur/<?=$couleur?>");
                        border: 1px solid #cacece;
                        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                        padding: 9px;
                        display: inline-block;
                        position: relative;
                      }

                      input[type=checkbox][name=couleurProduit]:checked {
                        -webkit-appearance: none;
                        border: 2px solid black;
                      }
                      </style>
                    <input class="couleurCheck" type="checkbox" name="couleurProduit"   value="<?="$couleur";?>"
                    ><br></input>
                    </div>

                    <?php if(!empty($couleur2)) { ?>
                        <div class="couleurP">
                          <style>
                          input[type=checkbox][name=couleurProduit2] {
                            -webkit-appearance: none;
                            background: url("../couleur/<?=$couleur2?>");
                            border: 1px solid #cacece;
                            box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                            padding: 9px;
                            display: inline-block;
                            position: relative;
                          }

                          input[type=checkbox][name=couleurProduit2]:checked {
                            -webkit-appearance: none;
                            border: 2px solid black;
                          }
                          </style>
                        <input type="checkbox" name="couleurProduit2" value="<?="$couleur2";?>"><br></input>
                        </div>
                    <?php } ?>


                    <?php if(!empty($couleur3)) { ?>
                        <div class="couleurP">
                          <style>
                          input[type=checkbox][name=couleurProduit3] {
                            -webkit-appearance: none;
                            background: url("../couleur/<?=$couleur3?>");
                            border: 1px solid #cacece;
                            box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                            padding: 9px;
                            display: inline-block;
                            position: relative;
                          }

                          input[type=checkbox][name=couleurProduit3]:checked {
                            -webkit-appearance: none;
                            border: 2px solid black;
                          }
                          </style>
                        <input type="checkbox" name="couleurProduit3" value="<?="$couleur3";?>"><br></input>
                        </div>
                    <?php } ?>


                    <?php if(!empty($couleur4)) { ?>
                        <div class="couleurP">
                          <style>
                          input[type=checkbox][name=couleurProduit4] {
                            -webkit-appearance: none;
                            background: url("../couleur/<?=$couleur4?>");
                            border: 1px solid #cacece;
                            box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                            padding: 9px;
                            display: inline-block;
                            position: relative;
                          }

                          input[type=checkbox][name=couleurProduit4]:checked {
                            -webkit-appearance: none;
                            border: 2px solid black;
                          }
                          </style>
                        <input type="checkbox" name="couleurProduit4" value="<?="$couleur4";?>"><br></input>
                        </div>
                    <?php } ?>

                    <?php if(!empty($couleur5)) { ?>
                        <div class="couleurP">
                          <style>
                          input[type=checkbox][name=couleurProduit5] {
                            -webkit-appearance: none;
                            background: url("../couleur/<?=$couleur5?>");
                            border: 1px solid #cacece;
                            box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                            padding: 9px;
                            display: inline-block;
                            position: relative;
                          }

                          input[type=checkbox][name=couleurProduit5]:checked {
                            -webkit-appearance: none;
                            border: 2px solid black;
                          }
                          </style>
                        <input type="checkbox" name="couleurProduit5" value="<?="$couleur5";?>"><br></input>
                        </div>
                    <?php } ?>

                    <?php if(!empty($couleur6)) { ?>
                        <div class="couleurP">
                          <style>
                          input[type=checkbox][name=couleurProduit6] {
                            -webkit-appearance: none;
                            background: url("../couleur/<?=$couleur6?>");
                            border: 1px solid #cacece;
                            box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                            padding: 9px;
                            display: inline-block;
                            position: relative;
                          }

                          input[type=checkbox][name=couleurProduit6]:checked {
                            -webkit-appearance: none;
                            border: 2px solid black;
                          }
                          </style>
                        <input type="checkbox" name="couleurProduit6" value="<?="$couleur6";?>"><br></input>
                        </div>
                    <?php } ?>

                    <?php if(!empty($couleur7)) { ?>
                        <div class="couleurP">
                          <style>
                          input[type=checkbox][name=couleurProduit7] {
                            -webkit-appearance: none;
                            background: url("../couleur/<?=$couleu72?>");
                            border: 1px solid #cacece;
                            box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                            padding: 9px;
                            display: inline-block;
                            position: relative;
                          }

                          input[type=checkbox][name=couleurProduit7]:checked {
                            -webkit-appearance: none;
                            border: 2px solid black;
                          }
                          </style>
                        <input type="checkbox" name="couleurProduit7" value="<?="$couleur7";?>"><br></input>
                        </div>
                    <?php } ?>

                    <?php if(!empty($couleur8)) { ?>
                        <div class="couleurP">
                          <style>
                          input[type=checkbox][name=couleurProduit8] {
                            -webkit-appearance: none;
                            background: url("../couleur/<?=$couleur8?>");
                            border: 1px solid #cacece;
                            box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
                            padding: 9px;
                            display: inline-block;
                            position: relative;
                          }

                          input[type=checkbox][name=couleurProduit8]:checked {
                            -webkit-appearance: none;
                            border: 2px solid black;
                          }
                          </style>
                        <input type="checkbox" name="couleurProduit8" value="<?="$couleur8";?>"><br></input>
                        </div>
                    <?php } ?>
                </div>
             </div>
             <button type="submit" class="ajoutPanier" name="button">Ajouter au panier</button>
           </form>
       </div>
             </div>

            <div class="detailPrix">
                <p><?=$prix?> €</p>
            </div>

            <?php
            if (isset($_SESSION['user'])){
              $emailU = $_SESSION['user'];

                if(isset($_POST['button']))
                {
                  if(isset($_POST['couleurProduit']))
                  {

                      $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                      $panier -> execute(array(
                          'product_idA' => $idA,
                          'user_email' => $emailU,
                          'product_idV' => '0',
                          'couleurProduit' => $_POST['couleurProduit'],
                          'Taille' => "Unique"
                      ));
                      unset($_POST['button']);
                  }
                  else if(isset($_POST['couleurProduit2']))
                  {
                    $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                    $panier -> execute(array(
                      'product_idA' => $idA,
                      'user_email' => $emailU,
                      'product_idV' => '0',
                      'couleurProduit' => $_POST['couleurProduit2'],
                      'Taille' => "Unique"
                    ));
                    unset($_POST['button']);
                  }
                  else if(isset($_POST['couleurProduit3']))
                  {
                    $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                    $panier -> execute(array(
                      'product_idA' => $idA,
                      'user_email' => $emailU,
                      'product_idV' => '0',
                      'couleurProduit' => $_POST['couleurProduit3'],
                      'Taille' => "Unique"
                    ));
                    unset($_POST['button']);
                  }
                  else if(isset($_POST['couleurProduit4']))
                  {
                    $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                    $panier -> execute(array(
                      'product_idA' => $idA,
                      'user_email' => $emailU,
                      'product_idV' => '0',
                      'couleurProduit' => $_POST['couleurProduit4'],
                      'Taille' => "Unique"
                    ));
                    unset($_POST['button']);
                  }
                  else if(isset($_POST['couleurProduit5']))
                  {
                    $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                    $panier -> execute(array(
                      'product_idA' => $idA,
                      'user_email' => $emailU,
                      'product_idV' => '0',
                      'couleurProduit' => $_POST['couleurProduit5'],
                      'Taille' => "Unique"
                    ));
                    unset($_POST['button']);
                  }
                  else if(isset($_POST['couleurProduit6']))
                  {
                    $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                    $panier -> execute(array(
                      'product_idA' => $idA,
                      'user_email' => $emailU,
                      'product_idV' => '0',
                      'couleurProduit' => $_POST['couleurProduit6'],
                      'Taille' => "Unique"
                    ));
                    unset($_POST['button']);
                  }
                  else if(isset($_POST['couleurProduit7']))
                  {
                    $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                    $panier -> execute(array(
                      'product_idA' => $idA,
                      'user_email' => $emailU,
                      'product_idV' => '0',
                      'couleurProduit' => $_POST['couleurProduit7'],
                      'Taille' => "Unique"
                    ));
                    unset($_POST['button']);
                  }
                  else if(isset($_POST['couleurProduit8']))
                  {
                    $panier = $db->prepare('INSERT INTO panier(product_idA, user_email, product_idV, couleurProduit, Taille)VALUES(:product_idA, :user_email, :product_idV, :couleurProduit, :Taille)');
                    $panier -> execute(array(
                      'product_idA' => $idA,
                      'user_email' => $emailU,
                      'product_idV' => '0',
                      'couleurProduit' => $_POST['couleurProduit8'],
                      'Taille' => "Unique"
                    ));
                    unset($_POST['button']);
                  }

                    header('Location: accessoires.php');
                }


            if(isset($_POST['buttonF'])){
                $panier = $db->prepare('INSERT INTO Favoris(Favo_idA, Favo_idV, user_email)VALUES(:Favo_idA, :Favo_idV, :user_email)');
                $panier -> execute(array(
                    'Favo_idA' => $idA,
                    'Favo_idV' => '0',
                    'user_email' => $emailU
                ));
                unset($_POST['buttonF']);
              }

              if(isset($_POST['buttonSupprFavo'])){
                $panier = $db->exec('DELETE FROM Favoris WHERE Favo_idA = '.$idA);
              }
            }
            else if (!isset($_SESSION['user']) AND (isset($_POST['button'])))
            {
            ?>
              <script>
                alert("<?php echo htmlspecialchars('Veuillez vous connecter pour ajouter un produit au panier !', ENT_QUOTES); ?>")
              </script>
            <?php
            }
        $idA_favo = $db -> prepare('SELECT Favo_idA FROM Favoris WHERE Favo_idA = ?');
        $idA_favo->execute(array($idA));
        $idA_favo = $idA_favo->fetch();
        $idA_favoris = $idA_favo['Favo_idA'];


        if(isset($_SESSION['user']))
        {

        if($idA_favoris == $idA)
        {
          ?>
            <div class="favoris">
                <form method="post" enctype="multipart/form-data">
                  <button type="submit" class="favoris" name="buttonSupprFavo" value="Suppr"><img src="../images/like.png"</button>
                </form>
            </div>
            <?php
        }
        else{
        ?>
          <div class="favoris">
              <form method="post" enctype="multipart/form-data">
                <button type="submit" class="favoris" name="buttonF" value="Ajout"><img src="../images/favoris.png"</button>
              </form>
          </div>
          <?php
        }
      }

?>
            <script type="text/javascript">
            var slideIndex = 1;
            showSlides(slideIndex);

            // Next/previous controls
            function plusSlides(n) {
            showSlides(slideIndex += n);
            }

            // Thumbnail image controls
            function currentSlide(n) {
            showSlides(slideIndex = n);
            }

            function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("dot");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            }
        </script>

<?php ob_end_flush(); ?>
    </body>
</html>
