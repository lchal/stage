<!DOCTYPE html>
<html>
  <head>
    <style>
      table{
        width: 100%;
      }
    </style>
  </head>
  <body>
    <h1>Facture</h1>
    <table>
      <thead>
        <th>Nom du produit</id>
        <th>Couleur du produit</id>
        <th>Taille</id>
        <th>Prix</id>
      </thead>

      <tbody>
        <?php
          $prixCom = 0;
          $prixTotal =0;
          function SupExt ($file_name) {
            $trouve_moi = ".";

            $position = strpos($file_name, $trouve_moi);
            return substr($file_name, 0, $position);
          }
          foreach ($commandesU as $commandesU):
            $Com_idA = $commandesU['Com_idA'];
            $Com_idV = $commandesU['Com_idV'];
            $email = $commandesU['email_user'];
            $couleurProduit = $commandesU['couleurProduit'];
            $couleurProduit = SupExt($couleurProduit);?>
            <tbody>
            <?php
              if($Com_idA !=0)
              {
                $accessoiresCommande= $db->query("SELECT * FROM accessoires WHERE idA = '$Com_idA'");
                $accessoiresCommande = $accessoiresCommande->fetchAll();
                foreach($accessoiresCommande as $accessoiresCommande):?>
                    <tr>
                        <td data-label="Amount"><?= $accessoiresCommande['nom'];?></td>
                        <td data-label="Amount"><?= ucfirst($couleurProduit);?></td>
                        <td data-label="Amount"><?= $commandesU['Taille'];?></td>
                        <td data-label="Amount"><?= $accessoiresCommande['prix'];?>€</td>
                    </tr>
                <?php
                $prixCom += $accessoiresCommande['prix'];
              endforeach;
              }

              if($Com_idV !=0)
              {
                $vetementsCommande= $db->query("SELECT * FROM vetements WHERE idV = '$Com_idV'");
                $vetementsCommande = $vetementsCommande->fetchAll();
                foreach($vetementsCommande as $vetementsCommande):?>
                    <tr>
                        <td data-label="Amount"><?= $vetementsCommande['nom'];?></td>
                        <td data-label="Amount"><?= ucfirst($couleurProduit);?></td>
                        <td data-label="Amount"><?= $commandesU['Taille'];?></td>
                        <td data-label="Amount"><?= $vetementsCommande['prix'];?>€</td>

                    </tr>
                <?php
              $prixCom += $vetementsCommande['prix'];
            endforeach;
              }?>
            </tbody>

        <?php
        endforeach; ?>
        <tfoot>
        </br></br></br></br>
          <tr>
            <th>Total HT</th>
            <th><?= $prixCom?></th>
         </tr>
         <?php
          $prixCom += $prixCom*0.05 + 0.7 + 4.5?>
         <tr>
           <th>Total TTC:</th>
           <th><?= number_format($prixCom,2)?></th>
         </tr>
        </tfoot>
    </table>
  </body>
</html>
