<?php
  session_start();
  require('../modele/connexionDB.php');
  $db = connectBDD();

  if(!isset($_SESSION['user']) || $_SESSION['admin'] == "0")
  {
    header('Location: ../controleur/accessoires.php');
    die();
  }

  if(isset($_GET['email']) AND !empty($_GET['email']) AND isset($_GET['cle']) AND !empty($_GET['cle']))
  {
    $getmail = $_GET['email'];
    $getcle = $_GET['cle'];
    echo $getcle;
    echo $getmail;
    $recupMail = $db->prepare('SELECT * FROM utilisateurs WHERE email = ? AND cle = ?');
    $recupMail -> execute(array($getmail, $getcle));
    if($recupMail -> rowCount() > 0)
    {
      $userInfo = $recupMail->fetch();
      if($userInfo['verifier'] !=  1)
      {
          $updateVerifCle = $db->prepare('UPDATE utilisateurs SET verifier = ? WHERE email = ? AND cle = ?');
          $updateVerifCle->execute(array(1, $getmail, $getcle));
      }
    }
  }
  header('Location: ../controleur/identification.php?reg_err=validé');die();


?>
