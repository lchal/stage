<?php

if(!isset($_SESSION['user']))
{

  header('Location: ../controleur/accessoires.php');
  die();
}
  $email = $_SESSION['user'];
  $info_user = $db -> query("SELECT * FROM utilisateurs where email = '$email'");
  $donnees = $info_user->fetchAll();
  foreach($donnees as $donnees):
    $pntFidelite=$donnees['pntFidelite'];
    $pseudo = $donnees['pseudo'];
    $email = $donnees['email'];
  endforeach;
?>
<head>
  <link type="text/css" rel="stylesheet" href="../css/vueInfoPerso.css">
</head>
<div class="login-form">
  <form method="post">
    <h4><label for="pseudo" class="fieldlabels">Votre pseudo: </label></h4>
    <h5 class="text-center" name="pseudo"><?=$pseudo?></h5></br>

    <h4><label for="email" class="fieldlabels">Votre email: </label></h4>
    <h5 class="text-center" name="email"><?=$email?></h5></br>

    <h4><label for="fid" class="fieldlabels">Vos points de fidelite: </label></h4>
    <h5 class="text-center"  name="fid"><?=$pntFidelite?></h5>
  </form>
</div>
