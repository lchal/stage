<script type="text/javascript">
function traiterTelephone(e) {
	var txtCarOk="0123456789";
	var car="";
	var isCarOk=false;
	var zone="";
	var txtAgent = navigator.userAgent.toLowerCase();
	var iePos = txtAgent.indexOf("msie");
	var ffPos = txtAgent.indexOf("firefox");
	if (iePos>=0) {
		car=String.fromCharCode(event.keyCode);
		if (txtCarOk.indexOf(car)>=0) {
			isCarOk=true;
		}
		var txt="";
		zone=event.srcElement;
	}
	//if (ffPos>=0) {
	else {
		car=String.fromCharCode(e.charCode);
		if (txtCarOk.indexOf(car)>=0) {
			isCarOk=true;
		}
		if ((e.charCode==0)&&(e.keyCode>0)) {
			return true;
		}
		zone=e.target;
	}
	if ((iePos<0)&&(ffPos<0)) {
		// Ignorer le gestionnaire pour les autres navigateurs
		//return true;
	}
	if (isCarOk) {
		if (zone.value.length<14) {
			zone.value+=car
			var nb=zone.value.length;
			if ((nb==2)||(nb==5)||(nb==8)||(nb==11)) { // Gestion du s�parateur
				zone.value+=".";
			}
		}
	}
	return false;
}
</script>

<html>
<link href="../css/valideCom.css" rel="stylesheet">
<h2 id="heading">Données personnelles</h2>
  <form id="formInfo" action="../controleur/valideCom_traitement.php" method="post">
		<div id="form-card">
			<label class="fieldlabels">Prénom: *</label>
				<input class="dp" type="text" name="Prenom" placeholder="Prénom" required autocomplete="off">

			<label class="fieldlabels">Nom: *</label>
				<input class="dp" type="text" name="Nom" placeholder="Nom" required autocomplete="off">

			<label class="fieldlabels">Adresse: *</label>
				<input class="dp" type="text" name="Adresse" placeholder="Adresse" required autocomplete="off">

			<label class="fieldlabels">Code Postal: *</label>
				<input class="dp" type="text" name="CP" placeholder="Code Postal" required autocomplete="off">

			<label class="fieldlabels">Téléphone: *</label>
				<input class="dp" type="text" name="Tel" placeholder="Téléphone" required autocomplete="off">

			<button type="submit" name="button" class="action-button">Valider</button>
		</div>
  </form>
  <script type="text/javascript">
  document.forms["formInfo"].elements["Tel"].onkeypress = traiterTelephone
  </script>
</html>
