<?php
    require('modele/connexionDB.php');
    $db = connectBDD();


    $vetement = $db -> prepare('SELECT * FROM vetements ORDER BY RAND() LIMIT 3');
    $accessoire = $db -> prepare('SELECT * FROM accessoires ORDER BY RAND() LIMIT 2');
    //exécution de la requête
    $executeisOk = $vetement -> execute();
    $executeestOk = $accessoire -> execute();

    //récupération des résultats
    $vetements = $vetement->fetchAll();
    $accessoires = $accessoire->fetchAll();

    ?>

    <html>
        <head>
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
        <link href="css/index.css" rel="stylesheet">
            <div class= "cadreP">
                <h1>Aperçu de nos produits:</h1>
                <?php foreach ($vetements as $vetements):
                    $photo=$vetements['photo'];?>

                    <a href="controleur/DetailVetements.php?idV=<?=$vetements['idV']?>">
                    <div class="container">
                            <div class="card">
                                <div class="card-header">
                                <?php echo "<img src=\"articles/$photo\" alt=\"rover\" />";?>
                                </div>


                                <div class="card-body">
                                  <span class="tag tag-teal"><?= $vetements['nom'] ?> </span>
                                  <h4>
                                      Prix : <?= $vetements['prix'] ?>€
                                  </h4>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>

                    <?php foreach ($accessoires as $accessoires):
                    $photo=$accessoires['photo'];?>


                    <a href="controleur/detailAccessoires.php?idA=<?=$accessoires['idA']?>">
                    <div class="container">
                        <div class="card">
                            <div class="card-header">
                            <?php echo "<img src=\"articles/$photo\" alt=\"rover\" />";?>
                            </div>


                            <div class="card-body">
                            <span class="tag tag-teal"><?= $accessoires['nom'] ?> </span>
                            <h4>
                                Prix : <?= $accessoires['prix'] ?>€
                            </h4>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </head>


</html>
